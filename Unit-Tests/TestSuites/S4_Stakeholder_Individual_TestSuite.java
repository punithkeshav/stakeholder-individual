/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S4_Stakeholder_Individual_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S4_Stakeholder_Individual_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //S4_StakeholderIndividual_Beta
    @Test
    public void S4_StakeholderIndividual_Beta() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Stakeholder Individual_Beta.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    

    //ISom-Qa_SA
      @Test
    public void S4_StakeholderIndividual_ISom_Qa_SA() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Stakeholder Individual_IsomQa.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  

     //ch05
      @Test
    public void S4_StakeholderIndividual_ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Stakeholder Individual_ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
      //ISom-DEV
      @Test
    public void S4_StakeholderIndividual_IsomDev() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Stakeholder Individual_IsomDev.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  

    //FR1-Capture_Stakeholder_Individual - MainScenario
    @Test
    public void FR1_Capture_Stakeholder_Individual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Stakeholder_Individual - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture_Stakeholder_Individual - AlternateScenario1
    @Test
    public void FR1_Capture_Stakeholder_Individual_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Stakeholder_Individual - AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture_Stakeholder_Individual - OptionalScenario
    @Test
    public void FR1_Capture_Stakeholder_Individual_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Stakeholder_Individual - OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_STA01_02_UpdateStakeholderDetails_MainScenario
    @Test
    public void UC_STA01_02_UpdateStakeholderDetails_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_STA01_02_UpdateStakeholderDetails_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1
     @Test
    public void UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2_CaptureStakeholderAnalysis_MainScenario
    @Test
    public void FR2_CaptureStakeholderAnalysis_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_CaptureStakeholderAnalysis_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   //FR2_Capture_Stakeholder_OptionalScenario
    
    @Test
    public void FR2_CaptureStakeholderAnalysis_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_Capture_Stakeholder_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
 //UC_STA02_02_CaptureTopicIssueAssessment_MainScenario 
    @Test
    public void UC_STA02_02_CaptureTopicIssueAssessment_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_STA02_02_CaptureTopicIssueAssessment_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario 
    @Test
    public void FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR3_Capture_and_or_UpdateRelatedStakeholders_AlternateScenario
    @Test
    public void FR3_Capture_and_or_UpdateRelatedStakeholders_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3_Capture_and_or_UpdateRelatedStakeholders_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4_CaptureRelatedGroups_MainScenario 
    @Test
    public void FR4_CaptureRelatedGroups_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR4_CaptureRelatedGroups_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR4_CaptureRelatedGroups_AlternateScenario
    @Test
    public void FR4_CaptureRelatedGroups_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR4_CaptureRelatedGroups_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_MainScenario
    @Test
    public void FR5_CaptureUpdateVulnerability_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5_CaptureUpdateVulnerability_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_AlternateScenario_1
    @Test
    public void FR5_CaptureUpdateVulnerability_AlternateScenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5_CaptureUpdateVulnerability_AlternateScenario_1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_AlternateScenario_2
    @Test
    public void FR5_CaptureUpdateVulnerability_AlternateScenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5_CaptureUpdateVulnerability_AlternateScenario_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6_AddViewEngagements_MainScenario
    @Test
    public void FR6_AddViewEngagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6_View_RelatedAssessment_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6_AddViewEngagements_AlternateScenario
    @Test
    public void FR6_AddViewEngagements_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6_AddViewEngagements_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7_AddViewEngagementPlan_MainScenario
    @Test
    public void FR7_AddViewEngagementPlan_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR7_AddViewEngagementPlan_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7_AddViewEngagementPlan_OptionalScenario
     @Test
    public void FR7_AddViewEngagementPlan_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR7_AddViewEngagementPlan_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     //FR8_AddViewCommitments_MainScenario
    @Test
    public void FR8_AddViewCommitments_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR8_AddViewCommitments_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR8_AddViewCommitments_AlternateScenario
    @Test
    public void FR8_AddViewCommitments_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR8_AddViewCommitments_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR9_AddViewGrievances_MainScenario
    @Test
    public void FR9_AddViewGrievances_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR9_View_Grievances_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR10_CaptureActions_MainScenario
    @Test
    public void FR10_CaptureActions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR10_CaptureActions_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR11_EditStakeholderIndividual_MainScenario
    @Test
    public void FR11_EditStakeholderIndividual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR11_EditStakeholderIndividual_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR12_DeleteStakeholderIndividual_MainScenario 
    @Test
    public void FR12_DeleteStakeholderIndividual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR12_DeleteStakeholderIndividual_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR13_ViewStakeholderIndividualReport_MainScenario
    @Test
    public void FR13_ViewStakeholderIndividualReport_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR13_ViewStakeholderIndividualReport_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR14_ViewStakeholderIndividualDashboard_MainScenario
    @Test
    public void FR14_ViewStakeholderIndividualDashboard_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR14_ViewStakeholderIndividualDashboard_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}