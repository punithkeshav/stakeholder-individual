/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Stakeholder_Individual_PageObjects extends BaseClass
{
    public static String Record_Number;
    public static String Window_1;
    
    
   public static String getWindow()
    {
        return Window_1;
    }

     public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }
     
    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }
    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }  
    
    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

   public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    
    public static String navigate_Stakeholders()
    {
        return "//label[text()='Stakeholder Management']";
    }
    
        public static String agencyDropDown()
    {
        return "//*[@id=\"control_7854D003-23E6-4A2E-AF2E-357C965FA684\"]/div[1]/a/span[2]/b[1]";
    }
        
       public static String agencyDropDown1()
    {
        return "//*[@id=\"control_5B580F56-394D-4695-8AB2-C2CB9AAE9EB9\"]/div[1]/a/span[2]/b[1]";
    }

    public static String navigate_StakeholderIndividual()
    {
        return "//label[contains(text(),'Stakeholder Individual')]";
    }

    public static String search()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[1]";
    }

    public static String SI_add()
    {
        
        return "/html/body/div[1]/div[3]/div/div[1]/div[2]/div[2]/div[1]";
        
    }
//div[@id='btnActApplyFilter']

    public static String si_fname()
    {
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }
    
      public static String natureOfContact()
    {
        return "//*[@id=\"control_17640BF1-096D-4917-93DC-398CE4D07B1C\"]/div[1]/div/input";
    }
        
      public static String engagmentDescription()
    {
        return "//*[@id=\"control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F\"]/div[1]/div/textarea";
    }
            
    public static String engagmentOutcome()
    {
        return "//*[@id=\"control_180C969D-A1C8-46FF-9F11-EA0457D2F762\"]/div[1]/div/textarea";
    }

    public static String si_lname()
    {
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }

    public static String si_knownas()
    {
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }

    public static String group_knownas()
    {
        return "//*[@id=\"control_5126FC78-97E6-49AA-A6D0-327CD4FD2CC5\"]/div[1]/div/input";
    }
    
    public static String si_title_dropdown()
    {
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//li";
    }

    public static String donotContactCheckbox()
    {
        return "//div[@id='control_75AD2A23-263F-4AE2-9F25-2EB4289FABE3']//div[@class='c-chk']//div";
    }
    
    public static String alternateContactDpdn()
    {
        return "//div[@id='control_EF9ED37D-3A57-4C98-9B4C-706C651662D8']//li";
    }

    public static String relationshipToStakeholder()
    {
        return "//div[@id='control_72C247C0-E132-44CC-BD6B-1D0509E66942']//li";
    }
            
    
    public static String exitButton()
    {
        return "//*[@id=\"form_E686D312-3E2F-4E66-9EAD-AC71C09267DD\"]/div[1]/i[2]";
    }
        
    
        public static String deleteRecordButton()
    {
        return "//*[@id=\"btnDelete_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD\"]/div";
    }
        
    public static String yesButton()
    {
        return "//*[@id=\"btnConfirmYes\"]/div";
    }
    
        public static String okButton()
    {
        return "(//div[@id='btnHideAlert']/div)[2]";
    } 
         
    public static String yesButton1()
    {
        return "//*[@id=\"btnConfirmYes\"]";
    } 
        
    public static String contactNumber()
    {
        return "(//div[@id='control_53EA1874-F86E-4399-938E-327B1C909030']//input)[1]";
    }
    
        public static String corrEmailAddress()
    {
        return "//*[@id=\"control_F8B44B7B-6FB2-4190-BD96-0D52ADD596F3\"]/div[1]/div/input";
    }
    
    public static String si_title(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_AltContact(String text)
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[13]/ul[1]/ul/li/a";
    }
  
    public static String Dob()
    {
     return"//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']/div/span/span/input";
        
    }
    
      public static String engagementStartDate()
    {
     return"//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']/div/span/span/input";
        
    }
     public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }
     
          public static String RecurrenceFrequency_Option(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }
          
      public static String WeekOfMonth_Dropdown()
    {  return "//*[@id=\"control_86F620B2-495B-46CC-B2D5-1A8A78A91096\"]/div[1]/a/span[2]/b[1]";

    }
      
          public static String AnnuallyFrom_Dropdown()
    {  
        return "//*[@id=\"control_7DA20857-9EFF-4817-A80B-ABD07F22AEED\"]/div[1]/a/span[2]/b[1]";

    }
    
    
          public static String Text3()
    {
        return "//*[@id=\"0ca81f20-e6e7-4072-af44-8e86fe3905c9_anchor\"]";
    }
     
    public static String Dob1(String text)
    {
        return "/html/body/div[4]/div[3]/div/div[2]/div[2]/div[4]/div[22]/div[9]/div[2]/div[2]/div/div[4]/div[1]/span/span";
        //return"//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']/div/span/span/input";
    }
    public static String si_relationshipOwner_dropdown()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

       public static String group_relationshipOwner_dropdown()
    {
        return "//*[@id=\"control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1\"]/div[1]/a/span[2]/b[1]";
    }

    
        public static String accountableOwner()
    {
        return "//div[@id='control_2A0EA9D1-04B4-4092-AD71-0CA6ED2D9C4B']/div/a/span[2]/b";
    }
    
        
     public static String group_accountableOwner()
    {
        return "//*[@id=\"control_A1C07241-404B-4E18-90D1-E7F20AB4E625\"]/div[1]/a/span[2]/b[1]";
    }
     public static String si_relationshipOwner_dropdownValue(String text)
    {
        return "//a[text()='" + text + "']";
    }
            
    public static String si_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String StakeholderCategoriesSelectAll()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }
    
    public static String si_detailsTab()
    {
        return "//li[@id='tab_DA528E34-BBB5-4777-A826-D546DBBB3A69']";
    }   
    
      public static String text_Search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }        
        
        public static String TextCheckBox(String text)
    {            
        
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']/i";
    }
        
       public static String TextCheckBox1()
    {            
        
       // return "/html/body/div[1]/div[3]/div/div[2]/div[8]/div[1]/input";
        return "//*[@id=\"7645f3c2-960c-4118-ab6b-0269b4a1ec74_anchor\"]/i[1]";
    }
       
       public static String TextCheckBox2()
    {            
        
       
        return "//*[@id=\"acc689e1-ed65-420b-9ccd-49ab775aa27c_anchor\"]/i[1]";
    }
       
     public static String text_Search1()
    {            
        
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    
    }

     
     
     
             
   public static String actionSave()
    {                 
        return "//*[@id=\"btnSave_form_3FA76381-27A7-4E29-B2EF-9BE41116F069\"]/div[3]";
    }
   
   
         public static String multiUserTickBox1()
    {                 
        return "//*[@id=\"control_9FB64F38-240A-4D57-8EBF-202D8124CDEE\"]/div[1]/div";
    }
         
         
       public static String actionDueDate()
    {                 
        return "//*[@id=\"control_A1A7A250-4916-472D-A6A5-CDA980F5DA52\"]/div[1]/span/span/input";
    }
       
        public static String multiUserDropDown()
    {                 
        return "//*[@id=\"control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033\"]/div[1]/a/span[2]/b[1]";
    }
             
             
        public static String getRecord_Number()
    {
        return Record_Number;
    }
        
      public static String SearchBox1()
    {            
        
        return "/html/body/div[1]/div[3]/div/div[2]/div[9]/div[1]/input";
    }
           
    public static String si_designation()
    {
        return "//div[@id='control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2']/div/div/input";
    }

    public static String si_dob()
    {
        return "//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']//input";
    }

    public static String si_engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    
    public static String si_age()
    {
        return "//div[@id='control_1BC13F9E-47D7-436E-A5BB-97E88B8D86F5']/div/input";
    }

    public static String si_idnum()
    {
        return "//div[@id='control_5A680F16-7C29-49C2-9698-68C906C1F35F']/div/div/input";
    }

     public static String si_primaryContactNum()
    {
        return "//div[@id='control_E47C7BB6-4A97-41C7-9E5D-C3C6FDA1B25C']/div/div/input";
    }
     
      public static String si_StreetAddress()
    {
        return "//div[@id='control_A02F0A7C-C785-431C-8D0C-921A485C8C97']//textarea";
    }
      
     public static String si_PostalCode()
    {
        return "//div[@id='control_F8B6B6D6-F042-40C2-8A54-08D8745DB284']/div/div/input";
    }
     
     public static String corr_PostalCode()
     {
     return "//*[@id=\"control_20F56E07-C58D-4269-9FD9-14A4A5903615\"]/div[1]/div/input";    
     }
     
    public static String si_Location()
    {
        return "//div[@id='control_B9F0F229-C6DB-440C-8E07-5EF10C11442E']//li";
    }
     
    public static String si_Location_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
   
     public static String si_TrainingManagementTab()
    {
        return "//li[@id='tab_098EB02D-6F88-4D36-9C78-3A50A1E3F20A']";
    }
    
     public static String si_HighestLevelOfEducation()
    {
        return "//div[@id='control_305523F4-2581-484F-B20F-983A40ACDE60']//li";
    }
     
    public static String si_HighestLevelOfEducation_select(String text)
    {
        return "//a[text()='" + text + "']";
    }
     public static String si_CurrentJobProfile()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']//li";
    }
     
     public static String si_CurrentJobProfile_select(String text)
    {
        return "//a[text()='" + text + "']";
    }
     
    
             
    public static String si_gender_dropdown()
    {
        return "//div[@id='control_BC0B3729-5DA7-493E-BA90-653F5C3E6151']//li";
    }

    public static String si_gender_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_nationality_dropdown()
    {
        return "//div[@id='control_447AC363-4DFF-4821-AB92-6695191EC822']";
    }

    public static String si_nationality_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String profile_tab()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[text()='Profile']";
    }

     public static String checkBoxTick()
    {
        //return "/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[20]/div[1]/a/span[2]/b[2]";
        return  "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']/div/a/span[2]/b";
    }
     
     
      public static String checkBoxTick1()
    {
        //return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']/div/a/span[2]/b[2]"; selects all
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']/div/a/span[2]/b";
    }
     public static String checkBoxTick2()
    {
      
        return"//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']/div/a/span[2]/b";
    }
     
      public static String checkBoxTick8()
    {
      
        return "//*[@id=\"control_B1893417-CAAD-4DC3-BD27-2EC339FBAE64\"]/div[1]/div";
    }
      
      
        public static String checkBoxTick6()
    {
        
        return "//*[@id=\"control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5\"]/div[1]/a/span[2]/b[1]";
    }
      
        
       public static String checkBoxTick7()
    {
        
        return "//*[@id=\"control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB\"]/div[1]/a/span[2]/b[1]";
    }
       
       
      public static String checkBoxTick3()
    {
       
        return "//*[@id=\"control_BDB3E74D-818E-4A51-8443-3F30BA7A472A\"]/div[1]/a/span[2]/b[1]";
    }
      
   
      
            public static String checkBoxTick4()
    {
       
        return "//*[@id=\"control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1\"]/div[1]/a/span[2]/b[1]";
    }
            
                  
     public static String checkBoxTick5()
    {
       
        return "//*[@id=\"control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0\"]/div[1]/a/span[2]/b[1]";
    }
      
    public static String si_stakeholdercat_selectall()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }

    public static String si_stakeholdercat(String text)
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']/div/a/span/ul/li";
    }
    
    public static String group_stakeholdercat(String text)
    {
        return "//*[@id=\"control_BDB3E74D-818E-4A51-8443-3F30BA7A472A\"]/div[1]/a/span[2]/b[1]";
    }

     public static String si_stakeholdercatDD()
    {
        return "//*[@id=\"control_F1357856-7A84-4716-9B1D-077C87CC8591\"]/div[1]/a/span[2]/b[1]";
    }
     
         public static String applicableBusinessUnitDD()
    {
        return "//*[@id=\"control_4CFB2165-708B-4D55-9988-9CDCB5487291\"]/div[1]/a/span[2]/b[1]";
    }
         
   public static String deselectAll()
    {
        return "//*[@id=\"control_4CFB2165-708B-4D55-9988-9CDCB5487291\"]/div[1]/a/span[2]/b[3]";
    }
     
        public static String businessUnitDD()
    {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']/div/a/span[2]/b";
    }
        
        public static String commitmentsBusinessUnitDD()
    {
        return "//*[@id=\"control_21A07758-2945-467A-92CF-F571AE83FEFA\"]/div[1]/a/span[2]/b[1]";
    }
        
      public static String functionalLocationDD()
    {
        return "//*[@id=\"control_7CB2DF1F-C8F7-40E7-B63C-BFD63ACBC20A\"]/div[1]/a/span[2]/b[1]";
    }
      
           public static String commitmentfunctionalLocationDD()
    {
        return "//*[@id=\"control_D3AF1650-B305-44CF-8463-1B003793EB72\"]/div[1]/a/span[2]/b[1]";
    }
           
           
    public static String si_appbu_selectall()
    {
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }

    public static String si_appbu_expand(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String si_appbu(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_impacttypes_selectall()
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }

    public static String impacttypes_selectall()
    {
        return "//div[@id='control_18DD1597-B800-4D1B-B063-A2E539BB3B8A']//b[@original-title='Select all']";
    }
    
    public static String contactEnquiryOrTopic()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//li";
    }
    
    public static String contactEnquiryOrTopicValue(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
     public static String location()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//li";
    }
    
    public static String locationValue(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
    
    public static String si_impacttypes(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_locationmarker()
    {
        return "//div[@id='control_6A7B2112-2526-41F1-893D-2702E81144D7']//div[@class='icon location mark mapbox-icons']";
    }

    public static String location_tab()
    {
        return "//div[text()='Location']";
    }

    public static String si_location()
    {
        return "jozi3.PNG";
    }

    public static String si_save()
    {
       return "//div[@id='btnSave_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
        //return "//*[@id=\"divMainNotif\"]";
    }
    
     public static String saveContinue()
    {
       return "//*[@id=\"control_ED751D41-E936-4042-8E41-FB030CA70377\"]/div[1]/div";
        
    }
    
     public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String dd_escape()
    {
        return "(//div[text()='Save'])[2]";
    }
    
    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox()
    {
        return "//div[@id='control_1F0A25D4-9905-4913-BC61-B1EED48CCCC6']//b[@original-title='Link to a document']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String si_savetocontinue()
    {
        return "//div[text()='Save supporting documents']";
    }

    public static String si_rightarrow()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String sik_rightarrow()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[22]/div[9]/div[1]/div[2]";
    }

    public static String sik_leftarrow()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[1]";
    }
    
    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String stakeholerDetails_Tab()
    {
        return "//div[text()='Stakeholder Details']";
    }
    
    public static String stakeholerAnalysis_Tab()
    {
        return "//div[text()='Stakeholder Analysis']";
    }

    public static String accessContactInfo_Checkbox()
    {
        return "(//div[@id='control_72B1FE8B-186F-4363-84AE-B844D349555E']//div)[2]";
    }

    public static String primaryContactNo()
    {
        return "(//div[@id='control_E47C7BB6-4A97-41C7-9E5D-C3C6FDA1B25C']//input)[1]";
    }

     public static String engagementPlanTitle()
    {
        return "//div[@id='control_185410E8-D077-4DE6-8958-5772CA36E091']/div/div/input";
    }
           
      public static String commitmentRegisterTitle()
    {
        return "//*[@id=\"control_804F813A-B603-4ED8-A416-3571D2146714\"]/div[1]/div/input";
    }
    
        public static String showInAdvance()
    {
        return "//div[@id='control_7929B98B-21B8-41E1-B4CF-873D96EC5263']/div/div/input";
    }
        
     public static String RecurrenceFrequency_tickBox()
    {
        return "//*[@id=\"control_D666E97A-5A99-4FA7-9B91-CC9CABDF948E\"]/div[1]/div";
    }
     
         public static String saveAndContinue()
    {
        return "//*[@id=\"control_B1893417-CAAD-4DC3-BD27-2EC339FBAE64\"]/div[1]/div";
    }

      public static String cSaveAndContinue()
    {
        return "//*[@id=\"control_D43881CB-1BF1-49AE-A3A1-95EC897D869F\"]/div[1]/div";
    }
    public static String secondaryContactNo()
    {
        return "(//div[@id='control_A7725878-35E5-4DA9-9D78-18F770172373']//input)[1]";
    }

    public static String emailAddress()
    {
        return "(//div[@id='control_D6050013-0413-45E9-87EB-942CD2CF9BA7']//input)[1]";
    }

    public static String streetAddress()
    {
        return "(//div[@id='control_A02F0A7C-C785-431C-8D0C-921A485C8C97']//textarea)[1]";
    }
    
     public static String _postalCode()
             
     {
       return "//*[@id=\"control_F8B6B6D6-F042-40C2-8A54-08D8745DB284\"]/div[1]/div/input" ;
    }


    public static String correspondence_Checkbox()
    {
        return "(//div[@id='control_DB350B2C-C01A-40C0-BD35-5BD957C9AE4D']//div)[2]";
    }
    
    public static String correspondence_Panel()
    {
        return "//span[text()='Correspondence Address']";
    }

    
    public static String correspondenceAddress()
    {
        return "(//div[@id='control_3CCEEABE-61CA-41A4-A905-05C200CE97BA']//textarea)[1]";
    }

    public static String comments()
    {
        return "(//div[@id='control_9EAB7FD6-BA53-4DAC-8CC9-435444A0C6C4']//textarea)[1]";
    }

     public static String TextBoxSearch()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[13]/div[1]/input";
    }
     
    public static String text_Search2()
    {
        return"//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

     public static String entity_tab()
    {
        return "//li[@id='tab_B82C2B7B-17B4-489B-A8EB-B3C93477CBBE']";
    }
    
     
       public static String relatedGroupsTab()
    {
        return "//li[@id='tab_B82C2B7B-17B4-489B-A8EB-B3C93477CBBE']";
    }
       
    public static String entity_add()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Add']";
    }

     public static String relatedGroups_add()
    {
        return "//*[@id=\"btnAddNew\"]/div";
    }
    public static String entity_name_dropdown()
    {
        return "//div[@id='control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D']//li";
    }
    
        public static String group_name_dropdown()
    {
        return "//*[@id=\"control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D\"]/div[1]/a/span[2]/b[1]";
    }

    public static String entity_name_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String stakeholder_positionDD()
    {
        return "//*[@id=\"control_6BE27956-6CE1-4F30-BB68-785A230BA898\"]/div[1]/a/span[1]/ul/li";
    }

    public static String stakeholderPosition_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
            
    public static String entity_save()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Save']";
    }

    public static String se()
    {
        return "//label[text()='Stakeholder Entity']";
    }

    public static String se_name()
    {
        return "(//td[text()='Entity name']/..//input)[2]";
    }

    public static String se_search()
    {
        return "//div[text()='Search']";
    }

    public static String se_select(String text)
    {
        return "//div[text()='" + text + "']";
    }

    public static String member()
    {
        return "//div[text()='Members']";
    }

    public static String members_wait()
    {
        return "(//div[text()='Drag a column header and drop it here to group by that column']/..//tr)[2]";
    }

    public static String isoHome()
    {
        return "//div[@class='iso header item brand']";
    }

    public static String left()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[1]";
    }

    
        public static String right()
    {
        return "//*[@id=\"control_38AB48FE-5C8D-401B-948C-9985EF810CED\"]/div[9]/div[1]/div[2]";
    }

    public static String engagementsTab()
    {
        return "//li[@id='tab_CF5F4EDE-1F5A-487F-9505-56097D794D34']";
    }
    
      public static String commitmentsTab()
    {
        return "//*[@id=\"tab_EDBD083D-736C-48FA-BE2B-0E6F86800AC0\"]/div[1]";
    }

      
      public static String searchOptions()
    {
        return "//*[@id=\"searchOptions\"]/div[1]/span[2]";
    }
            
    public static String searchButton1()
    {
        return "//*[@id=\"btnActApplyFilter\"]";
    }
    
    public static String selectReportbutton()
    {
        return "//div[@id='btnReports']/span";
    }
    
        public static String continueButton()
    {
        return "//div[@id='btnConfirmYes']/div";
    }

     public static String navigateButton()
    {
        return "//*[@id=\"#common-navigation\"]/vzl-button/div";
    }
     
        public static String appOverView()
    {
        return "//*[@id=\"rlui-popover-1\"]/div[1]/ul/div[1]/li[1]/div[1]";
    }
        
        
             
        public static String viewDashboard()
    {
        return "//*[@id=\"section_12b2ec63-6780-4722-9a13-55db66a6448c\"]/label";
    }
         
                
    public static String selectReportbutton1()
    {
        return "(//div[@id='report_50cdc151-6c9c-4d00-8685-857ab708d4bf']/span[3])[2]";
    }
        
       public static String viewReportbutton()
    {
        return "//*[@id=\"btnActApplyFilter\"]";
    }
     
            
    public static String engagementsRecordToBeOpened() {
        return "((//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div//table)[3]//tr)[1]";

    }
    
            
    public static String addEngagementsButton()
    {
        return "//div[@id='control_B7FD90BD-B7B7-45E8-9010-0F3DA43D264D']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

     public static String engagementTitle()
    {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
     
    public static String businessUnitTab()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//li";
    }

//    public static String businessUnit_SelectAll()
//    {
//        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
//    }

    public static String businessUnit_SelectAll()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@original-title='Select all']";
    }

    
    public static String businessUnitSelect(String text)
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String expandGlobalCompany()
    {
        return "//a[text()='Global Company']/../i";
    }

    public static String expandSouthAfrica()
    {
        return "//a[text()='South Africa']/../i";
    }

    public static String businessUnit_GlobalCompany()
    {
        return "//a[contains(text(),'Global Company')]";
    }

    public static String specificLocation()
    {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }

    public static String projectLink()
    {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }

    public static String projectTab()
    {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }

    public static String anyProject(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String cn_entity()
    {
        return "//div[text()='Create a new entity']";
    }
    
        public static String createNewStakeHolderGroup()
    {
        return "//div[text()='Create a new stakeholder group']";
    }

    public static String closeBusinessUnit()
    {
        return "(//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//span[2]//b[1])";
    }

    //FR4
//    public static String relationship_tab()
//    {
//        return "//div[text()='Relationships']";
//    }

    public static String relationship_tab()
    {
        return "//li[@id='tab_80C001EC-AB1F-4DA8-9B60-D1195CBF4D3B']";
    }
    
    public static String r_add()
    {
        return "//div[@id='control_32F8198B-15EA-4936-8A36-E7D8F25335E3']//div[text()='Add']";
    }

    public static String s_name_dropdown()
    {
        return "//div[@id='control_5F245AC7-D951-4043-BC9F-75A1DDEBA4F8']//li";
    }

    public static String s_name_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String tor_dropdown()
    {
        return "//div[@id='control_F2DF7289-3CBA-4ED1-8937-8C347D349930']//li";
    }

    public static String tor_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String r_save()
    {
        return "//div[@id='control_32F8198B-15EA-4936-8A36-E7D8F25335E3']//div[text()='Save']";
    }

    public static String new_individual()
    {
        return "//div[text()='Create a new individual']";
    }

    
    
   public static String addActionButton()
    {
        return "//*[@id=\"btnAddNew\"]/div";
    }      
            
    public static String actions_tab()
    {
        return "//div[text()='Actions']";
    }

    public static String a_add()
    {
        return "//div[@id='control_9C28A74C-46A0-4CA4-9378-F196C193BD8A']//div[text()='Add']";
    }

    public static String pf()
    {
        return "//div[@id='btnProcessFlow_form_3FA76381-27A7-4E29-B2EF-9BE41116F069']";
    }

    public static String Actions_desc()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Actions_deptresp_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }

    public static String Actions_deptresp_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }

    public static String Actions_respper_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }

    public static String Actions_respper_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[3]";
    }

    public static String Actions_ddate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

//    public static String Actions_save()
//    {
//        return "(//div[text()='Save'])[4]";
//    }

     public static String Actions_save()
    {
        return "//div[@id='btnSave_form_3FA76381-27A7-4E29-B2EF-9BE41116F069']";
    }
     
    
    
    public static String Actions_save2()
    {
        return "(//div[text()='Save'])[6]";
    }

    public static String Actions_savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String TM_tab()
    {
        return "//div[text()='Training Management']";
    }

    public static String cjp_dropdown()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']";
    }

    public static String cjp_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

//    public static String wh_tab()
//    {
//        return "(//div[text()='Work History'])[1]";
//    }

     public static String wh_tab()
    {
        return "//li[@id='tab_95ADAE33-1DDF-4B19-9259-999F62D0DF62']";
    }
     
    
    public static String wh_record()
    {
        return "//div[@id='control_4CC82A9F-308E-4CE2-AC57-E0B26DF8F468']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr";
    }
    
    public static String wh_record_close()
    {
        return "//div[@id='form_9AABEE75-5E60-48CD-AFD3-E42F607DC50D']//i[@class='close icon cross']";
    }
     

    
            
    public static String wh_wait()
    {
        return "//div[text()='Job profile']";
    }

    public static String wh_add()
    {
        return "//div[@id='control_4CC82A9F-308E-4CE2-AC57-E0B26DF8F468']//div[text()='Add']";
    }

    public static String wh_pf()
    {
        return "//div[@id='btnProcessFlow_form_9AABEE75-5E60-48CD-AFD3-E42F607DC50D']";
    }

    public static String jp_dropdown()
    {
        return "//div[@id='control_98786101-EED5-435E-80FE-79C026BF43C6']//li";
    }

    public static String jp_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }

    public static String seg_dropdown()
    {
        return "//div[@id='control_90A502E8-1540-4CCC-AD8B-A85F27F39444']//li";
    }

    public static String seg_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String wh_sdate()
    {
        return "//div[@id='control_53CAA00D-89E4-48DC-8D51-7651B7B78CB6']//input";
    }

    public static String wh_save()
    {
        return "//div[@id='btnSave_form_9AABEE75-5E60-48CD-AFD3-E42F607DC50D']";
    }

    public static String oh_tab()
    {
        return "//div[@id='divForms']//div[text()='Occupational Hygiene']";
    }

    public static String oh_record(String text)
    {
        return "//div[@id='control_11C9D3BC-24DE-47DB-92E6-D24DF3F2010F']//div[text()='" + text + "']";
    }

    public static String oh_wait()
    {
        return "//div[text()='Type of monitoring']";
    }


    public static String ShowMapLink()
    {
        return "//div[@id='control_6723362D-F9E2-457C-B125-13246382C3F7']/div[1]";
    }

    public static String engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }

    public static String communicationWithTab()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']";
    }

    public static String communicationWith_SelectAll()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']//b[@class='select3-all']";
    }

    public static String communicationUsers()
    {
        return "//a[contains(text(),'IsoMetrix Users')]";
    }

    public static String usersTab()
    {
        return "//div[@id='control_CD33B817-1DF7-4499-A055-2A70F29398FC']";
    }

    public static String userOption(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String stakeholdersTab()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']";
    }

    public static String stakeholdersTab2()
    {
        return "//div[text()='Stakeholder Details']";
    }

    public static String stakeholder_Array(String data)
    {
        return "(//a[text()='" + data + "']/i[1])";
    }

    public static String anyStakeholders(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String anyStakeholders2(String text, int counter)
    {
        return "(//a[contains(text(),'" + text + "')]/i[1])[ " + counter + " ]";
    }

    public static String Stakeholders_SelectAll()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//b[@class='select3-all']";
    }

    public static String entitiesTab()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']";
    }

    public static String entities(String data)
    {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String expandInPerson()
    {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }

    public static String expandOtherForms()
    {
        return "(//a[contains(text(),'Other forms of engagement')]//..//i)[1]";
    }

    public static String anyEngagementMethod(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String engagementMethodTab()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }

    public static String engagementPurposeTab()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//li";
    }

    public static String anyEngagementPurpose(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

//    public static String engagementTitle()
//    {
//        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
//    }

    public static String engagementDescription()
    {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }

    public static String engagementPlan()
    {
        return "(//div[@id='control_F082EFDC-2D10-478A-AB94-BD4A3FB4FE4B']//textarea)[1]";
    }

    public static String managementApprovalActionCheckBox()
    {
        return "(//div[@id='control_523CF0A9-803B-44F2-8572-BED13C022A03']/div)[1]";
    }

    public static String stakeholderApprovalAction()
    {
        return "(//div[@id='control_D0FFF631-1DA5-4BD3-A1C8-75A7527E6A34']/div)[1]";
    }

    public static String responsiblePersonTab()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']";
    }

    public static String numberOfAttendees()
    {
        return "(//div[@id='control_3D6CBB44-8CCF-4C7F-9150-D5DDD630995E']//input)[1]";
    }

    public static String anyResponsiblePerson(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String entitiesCloseDropdown()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']//b[@class='select3-down drop_click']";
    }

    public static String Engagement_save()
    {
        return "(//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[contains(text(),'Save')])[3]";
    }

    public static String projectTitle()
    {
        return "IsoMetrix";
    }

    public static String searchTab()
    {
        return "(//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div[contains(text(),'Search')])[1]";
    }

    public static String searchButton()
    {
        return "(//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div[contains(text(),'Search')])[2]";
    }

    public static String engagementsGridView(String data)
    {
        return "//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div//table//span[contains(text(),'" + data + "')]";
    }

    public static String Stakeholders_Close()
    {
        return "(//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//span[2]//b[1])";
    }

    public static String stakeholderEntity_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String getEngamentRecord()
    {
        return "//div[@class='recnum']//div[@class='record']";
    }

    public static String recordNo_Search()
    {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//td[text()='Record number']/..//input)[2]";
    }

    public static String getStakeholderIndividualRecord()
    {
        return "//div[@class='recnum']//div[@class='record']";
    }

    public static String si_RecordNo()
    {
        return "(//td[text()='Record number']/..//input)[2]";
    }

    public static String si_select(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String si_name()
    {
        return "(//td[text()='Full name']/..//input)[2]";
    }

    public static String si_status()
    {
        return "//div[@id='control_170F62D3-43C4-4548-A7C3-2E856B137AC8']//span[@class='select3-chosen']";
    }

    public static String si_statusSelect(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_businessunit(String data)
    {
        return "//a[text()='" + data + "']/i[1]";
    }

    public static String si_impacttype(String data)
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//a[text()='" + data + "']/i[1]";
    }

    public static String inspection_RecordSaved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String si_relationshipOwner()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//span[@class='select3-chosen']";
    }

    public static String si_si_relationshipOwnerSelect(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String categoryLabel()
    {
        return "//div[text()='Stakeholder categories']";
    }

    public static String designation()
    {
       return "//*[@id=\"control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2\"]/div[1]/div/input";
        //return "//div[2]/div[2]/div/div[2]/div/div/input";
    }

    
     public static String genderDD()
    {
        return "//*[@id=\"control_BC0B3729-5DA7-493E-BA90-653F5C3E6151\"]/div[1]/a/span[2]/b[1]";
    }

     
         public static String geographicZoneDD()
    {
        return "//*[@id=\"control_B9F0F229-C6DB-440C-8E07-5EF10C11442E\"]/div[1]/a/span[1]/ul/li";
    }
         
     public static String geographicSearch()
    {
        return "(//input[contains(@placeholder,'Type to search')])[12]";
    }
         
         
         
       public static String nationalityDD()
    {
        return "//*[@id=\"control_447AC363-4DFF-4821-AB92-6695191EC822\"]/div[1]/a/span[2]/b[1]";
    }

     
       public static String textSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

       
            public static String nationalitySearch()
    {
        return "(//input[contains(@placeholder,'Type to search')])[9]";
    }

       
      public static String genderSelect()
    {
        return "//li[@id='d2af7f03-7d4e-410d-bb45-f84f37234f81']//a";
    }
    
        public static String geographicSelect()
    {
        return "//*[@id=\"5f2115e3-c9c1-4d07-bde0-1a8ce9d19bd0_anchor\"]";
        
    }
          
        
       public static String geographicSelect1()
    {
        return "///*[@id=\"5f2115e3-c9c1-4d07-bde0-1a8ce9d19bd0_anchor\"]";
        
    }
      
      public static String nationalitySelect()
    {
        return "//*[@id=\"f9a5a651-a6c7-4476-b942-883255536baf_anchor\"]";
    }
    

    
    public static String addressPanel()
    {
        return "//div[@id='control_763E5982-3E9C-4FAE-9D02-647DF0AB255C']//div[@class='c-pnl-heading']";
    }

    public static String postalCode()
    {
        return "//div[@id='control_F8B6B6D6-F042-40C2-8A54-08D8745DB284']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String southAfricaArrow()
    {
        return "//a[text()='South Africa']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String gautengArrow()
    {
        return "//a[text()='Gauteng']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String jhbArrow()
    {
        return "//a[text()='Johannesburg']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectLocation(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String latitude()
    {
        return "//div[@id='control_44A782AF-225A-44F5-9434-F4E01DF62B38']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String longitude()
    {
        return "//div[@id='control_6E079358-9A1C-4480-B174-50B2D8D9097C']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String trainingManagementTab()
    {
        return "//div[text()='Training Management']";
    }

    public static String levelOfEducationDropdown()
    {
        return "//div[@id='control_305523F4-2581-484F-B20F-983A40ACDE60']//span[@class='select3-chosen']";
    }

    public static String levelOfEducationSelect(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String currentJobDropdown()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']//span[@class='select3-chosen']";
    }

    public static String currentJobSelect(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String doNotContactChkBox()
    {
        return "//div[@id='control_75AD2A23-263F-4AE2-9F25-2EB4289FABE3']/div[@class='c-chk']";
    }

    public static String alternateContact()
    {
        return "//div[@id='control_EF9ED37D-3A57-4C98-9B4C-706C651662D8']//span[@class='select3-chosen']";
    }

    public static String alternateContactSelect(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String relationShipToStakeholder()
    {
        return "//div[@id='control_72C247C0-E132-44CC-BD6B-1D0509E66942']//span[@class='select3-chosen']";
    }

    public static String relationShipToStakeholderSelect(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String homeButton()
    {
        return "//div[@class='iso header item brand']//div[text()='Solution Templates']";
    }

    public static String si_searchButton()
    {
        return "//div[@class='actionBar filter']//div[text()='Search']";
    }

    public static String selectRecord(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String contactNo()
    {
        return "//div[@id='control_53EA1874-F86E-4399-938E-327B1C909030']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String recordNoField()
    {
        return "(//input[@class='txt border'])[2]";
    }

    public static String socialStatusTab()
    {
        return "//span[text()='Social Status']";
    }

    public static String phoneLabel()
    {
        return "//div[text()='Phone number']";
    }

    public static String searchBtn()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String searchField()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[2]";
    }

    public static String selectRecorda(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String entitiesTabs()
    {
        return "//div[text()='Entities']";
    }

    public static String entitiesAdd()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Add']";
    }
    
    public static String entityNameDD()
    {
        return "//div[@id='control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D']//span[@class='select3-chosen']";
    }
    
    public static String entityNameSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }
    
    public static String stakeholderPositionDD()
    {
        return "//div[@id='control_DE361E8C-B66C-4402-B695-58413A1CA2BA']//span[@class='select3-chosen']";
    }
    
    public static String stakeholderPositionSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }
    
    public static String relatedSaveBtn()
    {
        return "//div[@id='btnSave_form_DF1172BD-686F-4776-A54F-2498D008B515']//div[text()='Save']";
    }

    public static String selectEntityRecord(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String selectOrderRecord(String data)
    {
       return "//div[text()='"+data+"']";
    }

    public static String employeesTab()
    {
        return "//div[text()='Employees']";
    }

    public static String selectRelatedRecord(String data)
    {
        return "//div[@id='control_196AE3D2-DDAF-4B03-BB99-6040D682878D']//div[text()='"+data+"']";
    }
    
    public static String searchFields()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[1]";
    }

    public static String addContractorBtn()
    {
        return "//div[text()='Add Contractor']";
    }

    public static String searchFieldIndividual()
    {
        return "(//div[@id='searchOptions']//td[@class='sel']//input[@class='txt border'])[1]";
    }

    public static String selectIndividualRecord(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String vulnerabilityTab()
    {
        return "//div[text()='Vulnerability']";
    }

    public static String vulnerabilityConfirmed()
    {
        return "//div[@id='control_AEDC9FCF-6D3C-441F-AF04-32727FF4FB69']//li";
    }
    
     public static String stakeholderVulnerable()
    {
        return "//*[@id=\"control_7C7D393B-7324-4B8C-807A-FE0E4CC95496\"]/div[1]/a/span[2]/b[1]";
    }

    public static String vulnerabilityConfirmedSelect(String data)
    {
       return "//a[text()='"+data+"']";
    }

    public static String vulnerabilityCategorization()
    {
        return "//div[@id='control_248B60E1-72E9-469B-82CF-DF61210D6790']//li";
    }

    public static String vulnerabilityCategorizationSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String vulnerabilityType()
    {
        return "//div[@id='control_2A441998-4C0E-41D7-99CD-F523407F28F0']//b[@class='select3-all']";
    }

    public static String vulnerabilityDescription()
    {
        return "//div[@id='control_2FAD1C82-911C-4258-B40B-8E3EB9C8B1F2']//textarea";
    }

    public static String location_Dropdown() {
        return "//div[@id='control_B9F0F229-C6DB-440C-8E07-5EF10C11442E']";
    }

    public static String step1_Registration() {
        return "//li[@id='tab_ECE1C32A-795E-42D7-8987-8720DBD32D6F']";
    }

    public static String recordSaved_popup() {
       // return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }
    public static String recordSaved_popup1() {
               return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
                //return "//*[@id=\"divMainNotif\"]";
    }
    public static String recordNoChanges_Saved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }
    
    //UC_01_03
    public static String si_analysisTab() {
        return "//li[@id='tab_3F5193FE-FB15-460C-9AD5-208966AC8E83']";
    }
    
    public static String stakeholderInterestDropdown() {
        return "//div[@id='control_31C7E767-EBB3-4298-91F8-6D2BB2087E27']//li";
    }
    
    public static String stakeholderInfluenceDropdown() {
        return "//div[@id='control_1925A4AA-1738-4B62-AEBE-52B8F6C31096']//li";
    }
    
    public static String stakeholderSupportDropdown() {
        return "//div[@id='control_60E6161A-A29E-4010-B97C-EF370B791A18']//li";
    }
    
    public static String impactOnStakeholderDropdown() {
        return "//div[@id='control_2179B78C-4EDC-41C2-8756-F54C65C1AA12']//li";
    }
    
    public static String commentsTextbox() {
        return "//div[@id='control_B1285D5A-AE37-4DC8-92F6-4FD64E3807A4']//textarea";
    }
       
    public static String singleSelect(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }
    
    public static String checklistSelect(String option) {
        return "(//a[text()='" + option + "']/i[1])";
    }
    
      public static String vulnerabiltyTypeDD() {
        return "//*[@id=\"control_2A441998-4C0E-41D7-99CD-F523407F28F0\"]/div[1]/a/span[2]/b[1]";
    }
    
//         public static String purposeOfEngagementDD() {
//        return "//*[@id=\"control_2A441998-4C0E-41D7-99CD-F523407F28F0\"]/div[1]/a/span[2]/b[1]";
//    }
    
      //*[@id="control_36951962-3063-4DA2-9846-ED7137AFC783"]/div[1]/a/span[2]/b[1]
         
            public static String EpurposeOfEngagementDD() {
        return "//*[@id=\"control_36951962-3063-4DA2-9846-ED7137AFC783\"]/div[1]/a/span[2]/b[1]";
    }

    //FR2 Topic or Issue Assessment
    public static String topicOrIssueAssessmentPanel() {
        return "//span[text()='Topic / Issue Assessment']";
    }
    
    public static String topicOrIssueAssessmentAddButton() {
        return "//div[@id='control_8FAC1D82-1B08-4953-9147-C9FD2CBDB738']//div[@id='btnAddNew']";
    }
    
    public static String topicIssueDropdown() {
        return "//div[@id='control_359049FA-9B56-4D59-A670-93070530E0E4']//li";
    }
    
    public static String influenceDropdown() {
        return "//*[@id=\"control_1925A4AA-1738-4B62-AEBE-52B8F6C31096\"]/div[1]/a/span[2]/b[1]";
    }
    
    
    public static String stakeholderSupportDD() {
        return "//*[@id=\"control_60E6161A-A29E-4010-B97C-EF370B791A18\"]/div[1]/a/span[2]/b[1]";
    }
        
    public static String stakeholderAnalysisComments() {
        return "//*[@id=\"control_B1285D5A-AE37-4DC8-92F6-4FD64E3807A4\"]/div[1]/div/textarea";
    }
      public static String guidelinesPanel() {
        return "//div[@id='control_95CB0DB1-9BE6-4D29-BA59-5C28A28C354E']/div[9]/div/span";
    }
        
    public static String interestDropdown() {
        return "//*[@id=\"control_31C7E767-EBB3-4298-91F8-6D2BB2087E27\"]/div[1]/a/span[2]/b[1]";
    }
    
    //FR6 Related Assessment
    public static String relatedAssessmentTab() {
        return "//li[@id='tab_C443EA0B-8F8D-4938-A62B-8FCFA50F6CB3']";
    }
    
    public static String relatedInspectionsRecordToBeOpened() {
        return "((//div[@id='control_D89FBDD4-85E9-4E14-BB42-7302BC5CCCA4']//div//table)[3]//tr)[1]";

    }
    
       public static String searchEngagements() {
        return "//div[@id='btnFilter']/div";

    }
       
      public static String addEngagements() {
        return "//div[@id='control_B7FD90BD-B7B7-45E8-9010-0F3DA43D264D']/div/div";

    }
       
      public static String searchEngagements1() {
        //return "//div[@id='btnActApplyFilter_C5D7993E-A223-4AE0-A15D-119FE22E21DC_smc_Previous_Engagements']/div";
        return "//*[@id=\"btnActApplyFilter_C5D7993E-A223-4AE0-A15D-119FE22E21DC_smc_Previous_Engagements\"]/div";
    }
    
    //FR9 View Grievances
    public static String grievancesTab() {
       // return "//li[@id='tab_1315A2E7-3411-4E1F-8424-CD79254AC639']";
        return "//*[@id=\"tab_1315A2E7-3411-4E1F-8424-CD79254AC639\"]/div[1]";
    }
    
    public static String grievancesRecordToBeOpened() {
        return "((//div[@id='control_26334270-5896-4852-918D-D508124D8B2D']//div//table)[3]//tr)[1]";

    }
    
      public static String typeOfAction_dropdown() {
       // return "//*[@id=\"control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE\"]/div[1]/a/span[2]/b[1]";
        return"//*[@id=\"control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE\"]/div[1]/a/span[2]/b[1]";
    }    
      
     public static String actionDescription() {
        return "//*[@id=\"control_1255F613-A69C-476A-8B05-4B87E5CA009F\"]/div[1]/div/textarea";
    }    

   public static String entityDropDown() {
        return "//*[@id=\"control_34D02E21-7837-484C-844E-BCC8CC077837\"]/div[1]/a/span[2]/b[1]";
    }  
   
      public static String responsibleDropDown() {
        return "//*[@id=\"control_7854D003-23E6-4A2E-AF2E-357C965FA684\"]/div[1]/a/span[2]/b[1]";
    }  

      
     public static String responsibleDropDown1() {
        return "//*[@id=\"control_7854D003-23E6-4A2E-AF2E-357C965FA684\"]/div[1]/a/span[2]/b[1]";
    }  
            
     public static String initiativesTab() {
        return "//li[@id='tab_DCF8E044-E466-4E2D-B21D-53E1B72C8984']";
    }
    
    public static String initiativesRecordToBeOpened() {
        return "((//div[@id='control_50E85B7D-AC6F-4FBD-9745-B4F4583650CD']//div//table)[3]//tr)[1]";

    }
    
    //FR10 Add Initiatives AS
    public static String addInitiativeButton()
    {
        return "//div[@id='control_553E1A4E-09B8-4A64-8B9D-A631D30F836E']";
    }
    
    public static String initaitivesProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    } 
    
    
    public static String selectAllDropdown() {
      return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//b[@original-title='Select all']";
    }
    
     public static String typeOfInitiativeDropdown()
    {
        return "//div[@id='control_2C68539F-B3A7-4844-A919-CCA67BB70A53']//li";
    }  
    
    public static String typeOfInitiativeDropdownValue(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])[1]";
    }  
    
    public static String typeOfInitiativeDropdownChildValue(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    } 
    
    public static String responsiblePersonDropdownValue(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    }  
    
    public static String initiativesSaveButton()
    {
        return "//div[@id='btnSave_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    } 
    
    
    
    //FR11 View Commitments MS
    public static String commitmentsTab1() {
        return "//li[@id='tab_EDBD083D-736C-48FA-BE2B-0E6F86800AC0']";
    }
    
    public static String commitmentsRecordToBeOpened() {
        return "((//div[@id='control_6E618AE2-27CC-4038-9A21-7EB7F68D9F0C']//div//table)[3]//tr)[1]";

    }
    
        public static String selectRecord() {
        return "//*[@id=\"grid\"]/div[3]/table/tbody/tr[1]/td[4]";

    }
        
        public static String selectRecord1() {
        return "//*[@id=\"grid\"]/div[3]/table/tbody/tr[2]/td[4]";

    }
    
    //FR11 Add Commitements AS
    public static String addCommitmentButton()
    {
        return "//div[@id='control_E679A8FB-562A-4E40-8531-3A1EF7DBC7CA']";
        //return"//div[@id='control_E679A8FB-562A-4E40-8531-3A1EF7DBC7CA']/div/div";
    }
    
     public static String iframeName()
    {
        return "ifrMain";
    }
     
     public static String commitementsProcessflow()
    {
        return "//div[@id='btnProcessFlow_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    } 
    
    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }
    
    public static String businessUnitDropdown()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//li";
    }  
      
        public static String EbusinessUnitDropdown()
    {
        return "//*[@id=\"control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5\"]/div[1]/a/span[2]/b[1]";
    }  
       
     public static String functionLocationDD()
    {
        return "//*[@id=\"control_B3768415-FC73-4B14-BA94-3D4E0299E15E\"]/div[1]/a/span[2]/b[1]";
    }     
    
     
     public static String purposeOfEngagementDD()
    {
        return "//*[@id=\"control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB\"]/div[1]/a/span[2]/b[1]";
    }   
          
     public static String methodOfEngagementDD()
    {
        return "//*[@id=\"control_4A471537-8229-4E54-A86C-DCEB99BA24D0\"]/div[1]/a/span[2]/b[1]";
    } 
     
         public static String EmethodOfEngagementDD()
    {
        return "//*[@id=\"control_5D729CA2-07EF-4E0A-B491-1E223A263474\"]/div[1]/a/span[2]/b[1]";
    } 
         
      public static String RecurrenceFrequency_Dropdown()
    {
        return "//*[@id=\"control_E457C6F1-F44C-4B21-8089-E7153700AFB4\"]/div[1]/a/span[2]/b[1]";
    } 
         
     
     public static String DayOfTheWeek_Dropdown()
    {
        return "//*[@id=\"control_D24F5B79-62C5-46E8-B38C-32AF14CCFD8C\"]/div[1]/a/span[2]/b[1]";
    } 
              
      public static String responsiblePersonDD()
    {
        return "//*[@id=\"control_213251A2-010A-4BBF-A65A-A1FC8C6F7033\"]/div[1]/a/span[2]/b[1]";
    } 
      
      public static String EresponsiblePersonDD()
    {
        return "//*[@id=\"control_C2B7C6FA-10FC-4593-BD27-6869D1790758\"]/div[1]/a/span[2]/b[1]";
    } 
      
      public static String cRegisterOwnerDD()
    {
        return "//*[@id=\"control_189B6C8F-E360-4339-A9B7-BF2C517A69E0\"]/div[1]/a/span[2]/b[1]";
    }
      
        public static String strategicObjectivesDD()
    {
        return "//*[@id=\"control_62842A63-2377-415B-AD40-F0552D08DB5B\"]/div[1]/a/span[2]/b[1]";
    }
     
        public static String EndDate()
    {
        return "//*[@id=\"control_56848732-729E-4C20-A0FC-1034ACF3D6F4\"]/div[1]/span/span/input";
    }
      
      public static String audienceMeetingTypeDD()
    {
        return "//*[@id=\"control_74C6DEB4-AE07-4476-AF24-753B7187F94A\"]/div[1]/a/span[2]/b[1]";
    } 
      
      public static String contactInquiryTopicDD()
    {
        return "//*[@id=\"control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019\"]/div[1]/a/span[2]/b[1]";
    } 
      
      
      public static String LinkedGrievancesDD()
    {
        return "//*[@id=\"control_ADF3F830-49E4-47FE-AB04-75C0890DA968\"]/div[1]/a/span[2]/b[1]";
    } 
      
      public static String LinkedCommitmentsDD()
    {
        return "//*[@id=\"control_8F98EBC3-4896-4198-9EE1-97CB806E57CB\"]/div[1]/a/span[2]/b[1]";
    } 
      
      public static String LinkedPermitsDD()
    {
        return "//*[@id=\"control_8F69A32A-57BE-4F05-B76A-1E97C9057B34\"]/div[1]/a/span[2]/b[1]";
    } 
      
       public static String geographicLocationDD()
    {
        return "//*[@id=\"control_F703A144-D0B6-4D4D-B5E2-D4E186427A43\"]/div[1]/a/span[2]/b[1]";
    }
                
    public static String expandChevron(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String businessUnitValue(String text)
    {
        return "(//a[text()='" + text + "']/i[1])";
    }
    
    public static String businessUnitDpdn()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//ul";
    } 
    
    public static String projectTilte()
    {
        return "(//div[@id='control_5CD5C423-3559-4155-A152-86D25E294254']//input)[1]";
    }
    
    public static String commencementDateXpath()
    {
         return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }
    
    public static String deliveryDateXpath()
    {
         return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }
    
    public static String approvedBudget()
    {
        return "(//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input)[1]";
    }
    
        
    public static String singleSelectIndex(String option,String index)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + option + "'])["+ index +"]";
    }
    
    public static String locationDropdown()
    {
        return "//div[@id='control_3A232C9E-B084-4A78-BED5-9FC568351142']//li";
    } 
    
    public static String responsiblePersonDropdown()
    {
        return "//div[@id='control_C1F24402-51E4-4F49-9520-00D62284121D']//li";
    } 
    
    public static String sectorDropdown()
    {
        return "//div[@id='control_6EF2C216-A531-46DB-806C-D5A5365021C9']//li";
    } 
    
    public static String description()
    {
        return " //div[@id='control_0889A1EE-89BA-4E41-B097-67F10CBD9C92']//textarea";
    }  
    
    public static String linkToMeetingDropdown()
    {
        return "//div[@id='control_E781027E-993C-4140-99E3-9A7B0EBF31C1']//li";
    } 
    
    public static String saveButton()
    {
        return " //div[@id='btnSave_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    } 
    
    //Fr12 View Resettlement MS
    public static String resettlementTab() {
        return "//li[@id='tab_2453C545-DBC9-46FF-B413-8ED01A728179']";
    }
    
    public static String resettlementRecordToBeOpened() {
        return "((//div[@id='control_24AAA983-0C6C-48A6-B2E4-C9618B44A376']//div//table)[3]//tr)[1]";

    }
    
    //FR13 View Land access
    public static String landaccessTab() {
        return "//li[@id='tab_3FF7EE9B-BAF9-495F-BD5F-D2A95307476A']";
    }
    
    public static String landaccessRecordToBeOpened() {
        return "((//div[@id='control_E363277C-112F-4470-9D17-394665FEE2D5']//div//table)[3]//tr)[1]";

    }
    
    //FR14 View Training Register
    public static String trainingRegisterRecordToBeOpened() {
        return "((//div[@id='control_C4F320E6-B864-4583-8884-78DE9F0A237F']//div//table)[3]//tr)[1]";

    }

    //FR15 View Occupational Hygiene MS
    public static String occuHygieneTab() {
        return "//li[@id='tab_D8AE8114-F78B-4A16-81D0-25F4651CB800']";
    }
    
    public static String occuHygieneRecordToBeOpened() {
        return "((//div[@id='control_11C9D3BC-24DE-47DB-92E6-D24DF3F2010F']//div//table)[3]//tr)[1]";

    }
    
    
     public static String engagementSearch() {
        return "(//div[@id='btnFilter']/div)[2]";
    }
     
       public static String engagementSearch1() {
        return "//div[@id='btnActApplyFilter_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F_smc79']/div";
    }
    
    //FR8 View Engagement plan MS
    public static String engagementPlanTab() {
        return "//li[@id='tab_33FB5A99-78F6-4DB1-ADDA-7985DDECFA98']";
    }
    
    
    //commitSearch
        public static String commitSearch() {
        return "//*[@id=\"btnFilter\"]/div";
    }
       
        
    public static String grievancesSearch() {
        return "(//div[@id='btnFilter']/div)[2]";
    }
        
     public static String grievancesSearch1() {
        return "//div[@id='btnActApplyFilter_B6196CB4-4610-463D-9D54-7B18E614025F_smc_Previous_Grievances']/div";
    }
        public static String commitSearch1() {
        return "//div[@id='btnActApplyFilter_7C062343-94C0-4691-9D16-7E07735C641C_smc179']/div";
    }
    
    public static String engagementPlanRecordToBeOpened() {
        return "((//div[@id='control_A19A1235-4486-4EB5-A9E4-EF24634429BF']//div//table)[3]//tr)[1]";

    }
    
    //FR8 Add Engagement plan AS
    public static String addEngagementPlanButton() {
        return "//div[@id='control_9AE170B5-C31D-4C0C-AAAD-EDC0EB7E230E']/div/div";
    }
    
    public static String engagementPlanProcess_flow() {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }
    
     public static String addCommitmentsProcess_flow() {
        return "//*[@id=\"btnProcessFlow_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2\"]/span";
    }
   
    public static String engagementPlanStartDate() {
        return "//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']//input[@type='text']";
    }
    public static String planBusinessUnitTab() {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']";
    }
    public static String businessUnit_SelectAllBtn(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String EP_projectLink() {
        return "//div[@id='control_DE6A7B48-B355-48A7-AA3E-8475171708AF']/div[1]";
    }
    public static String EP_projectTab() {
        return "//div[@id='control_301410A3-9118-4FCA-93B7-4C2A90320266']";
    }
    public static String frequency() {
        return "//div[@id='control_0189D12C-09E0-4D04-964C-34044E11A982']";
    }
    public static String EP_engagementPurposeTab() {
        return "//div[@id='control_36951962-3063-4DA2-9846-ED7137AFC783']";
    }
    public static String purposeOfEngagement(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String methodOfEngagement() {
        return "//div[@id='control_5D729CA2-07EF-4E0A-B491-1E223A263474']";
    }
    public static String methodOfEngagementSelect(String data) {
        return "//ul[@class='jstree-children']//a[text()='Project Update']";
    }
    public static String personResponsibleTab() {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']";
    }
    public static String EP_SaveBtn() {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']//div[text()='Save']";
    }        
    
    public static String engagementPlanEndDate() {
        return "//div[@id='control_56848732-729E-4C20-A0FC-1034ACF3D6F4']//input[@type='text']";
    }
    
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    
    public static String projectSelct(String data) {
        return "//a[text()='" + data + "']";
    }
    
     public static String engagementMethodArrow() {
        return "//a[text()='In-person engagements']/../i";
    }
    public static String responsiblePersonSelect(String data) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + data + "')]";
    }
    
    public static String SaveToContinue_SaveBtn() {
        return "//div[@id='control_B1893417-CAAD-4DC3-BD27-2EC339FBAE64']//div[text()='Save to continue']";
    }
    
    static String engagementPlanTitle;
    
    public static void setEngagementPlanTitle(String value) {
        engagementPlanTitle = value;
    }
    public static String getEngagementPlanTitle() {
        return engagementPlanTitle;
    }
    
    //FR18 View SI report
    public static String si_reports1() {
        return "//div[@id='btnReports_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']//span[@original-title='Reports']";
    } 
    
    
    public static String si_reports2() {
        return "(//div[@id='report_304CB37E-9C26-480A-BDAB-853E9B71750B']//span[@class='panelbarReportIcons icon chart-bars'])[2]";
    } 
    
    public static String popup_conf(){
        return "//div[@id='btnConfirmYes']";
    }
}
