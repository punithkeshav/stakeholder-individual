/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author LDisemelo
 */
public class MailSlurper_PageObjects {
 
    public static String mailSlurper()
    {
        return "http://saazu-ang-pd1.isometrix.net:8080/login";
    }

    public static String Username()
    {
        return "//input[@id='userName']";
    }

    public static String Password()
    {
        return "//input[@id='password']";
    }

    public static String LoginBtn()
    {
        return "//button[@id='btnSubmit']";
    }

    public static String recordNo_Search()
    {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//td[text()='Record number']/..//input)[2]";
    }

    public static String searchBtn()
    {
        return"//button[@id='btnSearch']";
    }

    public static String SubjectOrMessage()
    {
        return"//input[@id='txtMessage']";
    }

    public static String search_Btn()
    {
        return"//button[@id='btnExecuteSearch']";
    }

    public static String recordLink(String text)
    {
       return "//a[contains(text(),'#"+text+"')]";
    }

    public static String linkBackToRecord()
    {
       return "//a[contains(text(),'Link back to record')]";
    }

    
    

}
