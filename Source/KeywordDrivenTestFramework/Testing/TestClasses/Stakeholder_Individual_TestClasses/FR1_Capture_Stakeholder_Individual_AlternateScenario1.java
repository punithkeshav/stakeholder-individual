/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.Stakeholder_Individual_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Individual - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR1_Capture_Stakeholder_Individual_AlternateScenario1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR1_Capture_Stakeholder_Individual_AlternateScenario1()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!createNewIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean navigateToStakeholderIndividual(){
        //Navigate to Stakeholders
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");
        
      
        SeleniumDriverInstance.pause(10000);

        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.SI_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SI_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean createNewIndividual(){

        //Process flow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_fname())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
            
        }
       
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_fname(), testData.getData("First name"))){
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        } 
        
         narrator.stepPassedWithScreenShot("Successfully entered Individual first name.");
         
        //Stakeholder Individual last name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_lname())){
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_lname(), testData.getData("Last name"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual last name.");
        
        //Stakeholder Individual known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_knownas())){
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        } 
        
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual known as.");
        
        //Stakeholder Individual title dropdown
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to select '"+testData.getData("Title")+"' from 'Title' options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered 'text fields' .");
        
        //Relationship owners
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        
        SeleniumDriverInstance.pause(5000);
        
        //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search()))
            {
                error = "Failed to wait for Relationship owner text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Relationship owner")))
            {
                 error = "Failed to enter Relationship owner option :" + getData("Relationship owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        
        //Try this
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))) {
         error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' options.";
            return false;
        }
        //block this tickbox
           SeleniumDriverInstance.pause(5000);
  
        //Select after checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick())){
            error = "Failed to click Profile tab.";
            return false;
                   }
 
         SeleniumDriverInstance.pause(8000);
         
         //Accountable owner
         
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.accountableOwner())){
            error = "Failed to wait for 'Accountable owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.accountableOwner())){
            error = "Failed to click 'Accountable owner' dropdown.";
            return false;
        } 
         
       if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Accountable Owner")))
            {
                 error = "Failed to enter Accountable Owner option :" + getData("Accountable Owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
          //  pause(10000);
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to wait for Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to click Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }

         
      //Stakeholder Individual categorie 1
   if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercatDD())){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercatDD())){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 1")))
      
            {
                 error = "Failed to enter Categorie 1 :" + getData("Categorie 1");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        
             
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))) {
          error = "Failed to select '"+testData.getData("Categorie 1")+"' from 'Categorie 1' options.";
            return false;
        }
        pause(5000);
        
         //Select drop click
         
//         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick1())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick1())){
            error = "Failed to click Profile tab.";
            return false;
                   }
     narrator.stepPassedWithScreenShot("Successfully completed Stakeholder Individual categorie 1");
     
        //Applicable business units
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 2")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Categorie 2");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 2");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 2");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
             
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
            return false;
        }    

 if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
            return false;
        }
 
  if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to click Applicable business units option." + getData("Categorie 5");
            return false;
        }
//
        pause(5000);
        
         //Drop click escape
         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick2())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick2())){
            error = "Failed to click Profile tab.";
            return false;
                   }
        
        narrator.stepPassedWithScreenShot("Successfully completed Applicable business units");
        pause(5000);
         //Save and Continue
         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click Save and Continue.";
            return false;
                   }
        // pause(5000);
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
        
    if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
 String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Stakeholder Individual record saved");
return true;
}

}