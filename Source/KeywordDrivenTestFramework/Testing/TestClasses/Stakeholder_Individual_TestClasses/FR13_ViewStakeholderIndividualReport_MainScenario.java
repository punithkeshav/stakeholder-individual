/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Report viewed",
        createNewBrowserInstance = false
)

public class FR13_ViewStakeholderIndividualReport_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR13_ViewStakeholderIndividualReport_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!viewReport())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean viewReport(){
           
      //Navigate to Stakeholders
         
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");
        
        pause(4500);
        //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to click Search Button";
            return false;
        }         
        
        pause(4500);
        
       //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to click Search Button";
            return false;
        }         
        narrator.stepPassedWithScreenShot("Successfully clicked Search Button");
        pause(4030);
        
                
        //select report
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.selectReportbutton())){
            error = "Failed to wait for report Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.selectReportbutton())){
            error = "Failed to click report Button";
            return false;
        }         
         pause(4030);
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.selectReportbutton1())){
            error = "Failed to wait for report Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.selectReportbutton1())){
            error = "Failed to click report Button";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully clicked Search Button");
        
        SeleniumDriverInstance.switchToTabOrWindow();
        
          
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.continueButton())){
            error = "Failed to wait for report Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.continueButton())){
            error = "Failed to click report Button";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully clicked report Button");
        
        pause(5000);
        
        SeleniumDriverInstance.switchToTabOrWindow();
        
        pause(15500);
        return true;
    }

}
