/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.Stakeholder_Individual_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Add Engagments",
        createNewBrowserInstance = false
)

public class FR6_AddViewEngagements_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR6_AddViewEngagements_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH,2500);
       endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!navigateToEnagements())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean navigateToEnagements(){
         pause(3500);
         
         //Navigate to the engagements.

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }         
        
        pause(3000);
        
        //Add Engagments
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addEngagements())){
            error = "Failed to wait for Search engagements to be opened";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addEngagements())){
            error = "Failed to click Search engagements to be opened";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully click Search engagements");
         
        pause(5000);
        
        
         if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          
           //Purpose of engagement 
        //Engagmente Title
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementTitle())){
            error = "Failed to wait for Search engagements to be opened";
            return false;
        }
        
      if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementTitle(), getData("Engagement title"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully entered Engagement title.");
      
        //Engagement Date
        
         if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Engagement Date.";
            return false;
        }
          narrator.stepPassedWithScreenShot("DoB : '" + getData("Engagement Date") + "'.");
            
       //Business Unit
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EbusinessUnitDropdown())){
            error = "Failed to wait for business unit.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.EbusinessUnitDropdown())){
            error = "Failed to click business unit.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Business unit")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Business unit");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Business unit"))))
        {
            error = "Failed to wait for business units source drop down option : " + getData("Business unit");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Business unit"))))
        { 
            error = "Failed to click business units drop down option : " + getData("Business unit");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
             
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
            return false;
        }    

 if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
            return false;
        }
 
  if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to click Applicable business units option." + getData("Categorie 5");
            return false;
        }
  
           if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick6())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick6())){
            error = "Failed to click Profile tab.";
            return false;
                   }
        
        narrator.stepPassedWithScreenShot("Successfully completed Applicable business units");
pause(2500);

 
  
  //Functional location
  
  if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.functionLocationDD())){
            error = "Failed to wait for Functional location.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.functionLocationDD())){
            error = "Failed to click Functional location.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Functional location text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Functional location")))
            {
                 error = "Failed to enter Functional location option :" + getData("Functional location");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
        
        
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to click Functional location drop down option : " + getData("Functional location");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered Functional location.");
        pause(2500);
 //Purpose of engagement
 
 if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.purposeOfEngagementDD())){
            error = "Failed to wait for Purpose of engagement.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.purposeOfEngagementDD())){
            error = "Failed to click Purpose of engagement.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Purpose of engagement text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Purpose of engagement")))
            {
                 error = "Failed to enter Purpose of engagement option :" + getData("Purpose of engagement");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Purpose of engagement")))){
            error = "Failed to wait for 'Purpose of engagement' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Purpose of engagement")))) {
         error = "Failed to select '"+testData.getData("Purpose of engagement")+"' from 'Purpose of engagement' options.";
            return false;
        }
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick7())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick7())){
            error = "Failed to click Profile tab.";
            return false;
                   }
 narrator.stepPassedWithScreenShot("Successfully entered Purpose of engagement.");
 pause(2500);
 
 //Method of engagement
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.methodOfEngagementDD())){
            error = "Failed to wait for PMethod of engagement.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.methodOfEngagementDD())){
            error = "Failed to click Method of engagement.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Method of engagement text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Method of engagement")))
            {
                 error = "Failed to enter Method of engagement option :" + getData("Method of engagement");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
         
                 
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Method of engagement"))))
        {
            error = "Failed to wait for Method of engagement drop down option : " + getData("Method of engagement");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Method of engagement"))))
        {
            error = "Failed to click Method of engagement drop down option : " + getData("Method of engagement");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Method of engagement.");
        pause(2500);
 
 //Responsible person
 if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.responsiblePersonDD())){
            error = "Failed to wait for Responsible person.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.responsiblePersonDD())){
            error = "Failed to click Responsible person.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search()))
            {
                error = "Failed to wait for Responsible person text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Responsible person")))
            {
                 error = "Failed to enter Responsible person option :" + getData("Responsible person");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
         
                 
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        
 narrator.stepPassedWithScreenShot("Successfully entered Responsible person.");
 pause(2500);
 //Audience / Meeting type
    if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.audienceMeetingTypeDD())){
            error = "Failed to wait for Audience / Meeting type.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.audienceMeetingTypeDD())){
            error = "Failed to click Audience / Meeting type.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Audience / Meeting type text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Audience / Meeting type")))
            {
                 error = "Failed to enter Audience / Meeting type option :" + getData("Audience / Meeting type");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Audience / Meeting type")))){
            error = "Failed to wait for 'Audience / Meeting type' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Audience / Meeting type")))) {
         error = "Failed to select '"+testData.getData("Audience / Meeting type")+"' from 'Audience / Meeting type' options.";
            return false;
        }
 narrator.stepPassedWithScreenShot("Successfully entered Audience / Meeting type.");
 pause(2500);
 
 SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.contactInquiryTopicDD());
 //Contact inquiry / topic
 
  if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.contactInquiryTopicDD())){
            error = "Failed to wait for Contact inquiry / topic.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.contactInquiryTopicDD())){
            error = "Failed to click Contact inquiry / topic.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Contact inquiry / topic text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Contact inquiry / topic")))
            {
                 error = "Failed to enter Contact inquiry / topic option :" + getData("Contact inquiry / topic");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Contact inquiry / topic")))){
            error = "Failed to wait for 'Contact inquiry / topic' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Contact inquiry / topic")))) {
         error = "Failed to select '"+testData.getData("Purpose of engagement")+"' from 'Contact inquiry / topic' options.";
            return false;
        }
 narrator.stepPassedWithScreenShot("Successfully entered Contact inquiry / topic.");
 pause(2500);
 
 //Nature of contact inquiry / topic
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.natureOfContact())){
            error = "Failed to wait for 'Nature of contact inquiry / topic' input field.";
            return false;
            
        }
       
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.natureOfContact(), testData.getData("Nature of contact inquiry / topic"))){
            error = "Failed to click 'Nature of contact inquiry / topic' input field.";
            return false;
        } 
        
         narrator.stepPassedWithScreenShot("Successfully entered Nature of contact inquiry / topic.");
 
 
 
 pause(2500);
 //Engagement description
 
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagmentDescription())){
            error = "Failed to wait for 'Engagement description' input field.";
            return false;
            
        }
       
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagmentDescription(), testData.getData("Engagement description"))){
            error = "Failed to click 'SEngagement description' input field.";
            return false;
        } 
        
         narrator.stepPassedWithScreenShot("Successfully entered Engagement description.");
 
 pause(2500);
 
// Engagement outcome
  
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagmentOutcome())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
            
        }
       
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagmentOutcome(), testData.getData("Engagement outcome"))){
            error = "Failed to click 'Engagement outcome' input field.";
            return false;
        } 
        
         narrator.stepPassedWithScreenShot("Successfully entered Engagement outcome.");
pause(2500);

//Linked grievances

if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkedGrievancesDD())){
            error = "Failed to wait for Linked grievances.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.LinkedGrievancesDD())){
            error = "Failed to click Linked grievances.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Linked grievances text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Linked grievances")))
            {
                 error = "Failed to enter Linked grievances :" + getData("Linked grievances");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked grievances")))){
            error = "Failed to wait for 'Linked grievances' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked grievances")))) {
         error = "Failed to select '"+testData.getData("Linked grievances")+"' from 'Linked grievances' options.";
            return false;
        }
narrator.stepPassedWithScreenShot("Successfully entered Linked grievances.");
pause(2500);
//Linked commitments

if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkedCommitmentsDD())){
            error = "Failed to wait for Linked commitments.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.LinkedCommitmentsDD())){
            error = "Failed to click Linked commitments.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Linked commitments text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Linked commitments")))
            {
                 error = "Failed to enter Linked commitments option :" + getData("Linked commitments");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked commitments")))){
            error = "Failed to wait for 'Linked commitments' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked commitments")))) {
         error = "Failed to select '"+testData.getData("Linked commitments")+"' from 'Linked commitments' options.";
            return false;
        }
 

narrator.stepPassedWithScreenShot("Successfully entered Linked commitments.");
pause(2500);

SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.LinkedPermitsDD());
//Linked permits

if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkedPermitsDD())){
            error = "Failed to wait for Linked permits.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.LinkedPermitsDD())){
            error = "Failed to click Linked permits.";
            return false;
        }
        
          //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Linked permits text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Linked permits")))
            {
                 error = "Failed to enter Linked permits option :" + getData("Linked permits");
                return false;
            }

         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
         pause(2500);
 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked permits")))){
            error = "Failed to wait for 'Linked permits' options.";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Linked permits")))) {
         error = "Failed to select '"+testData.getData("Linked permits")+"' from 'Linked permits' options.";
            return false;
        }
narrator.stepPassedWithScreenShot("Successfully entered Linked permits.");
pause(2500);
//Geographic location
 if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.geographicLocationDD())){
            error = "Failed to wait for Geographic location.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.geographicLocationDD())){
            error = "Failed to click Geographic location.";
            return false;
        }
       
  if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.textSearch(), getData("Geographic location"))){
                error = "Failed to enter '" + getData("Geographic location") + "' into 'Geographic location' input field.";
                return false;
            } 
                 
          if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.geographicSelect()))
        {
            error = "Failed to click Function drop down option : " + getData("Geographic location");
            return false;
        }
narrator.stepPassedWithScreenShot("Successfully entered Geographic location.");
pause(3500);

//Save
if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.saveContinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.saveContinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }

SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.addEngagements());
pause(2500);
narrator.stepPassedWithScreenShot("Successfully added engagement.");


        return true;
    }

}
