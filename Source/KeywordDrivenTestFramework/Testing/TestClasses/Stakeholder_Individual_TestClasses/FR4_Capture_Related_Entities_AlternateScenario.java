/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add new Stakeholder Group",
        createNewBrowserInstance = false
)

public class FR4_Capture_Related_Entities_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_Capture_Related_Entities_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!createANewGroup()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean createANewGroup(){
        pause(5000);
  //Related Groups Tab

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relatedGroupsTab())){
            error = "Failed to wait for 'Entities' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relatedGroupsTab())){
            error = "Failed to click 'Entities' tab.";
            return false;
        }        
        pause(2000);
        
        //Create a new Stakeholder group
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.createNewStakeHolderGroup())){
            error = "Failed to wait for 'Create new entity' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.createNewStakeHolderGroup())){
            error = "Failed to click 'Create new entity' button.";
            return false;
        }
        
        pause(5000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          

         //Process flow 
    
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process flow.");
          //Group Name
   
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_name())){
            error = "Failed to wait for 'Group name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.group_name(), testData.getData("Group name"))){
            error = "Failed to enter '"+testData.getData("Group name")+"' into 'Group name' input.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered Group name.");
        
        //known As
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.group_knownas())){
            error = "Failed to wait for 'Stakeholder Group Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.group_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Group Known as' input field.";
            return false;
        } 
        
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual known as.");
        
        //Stakeholder categories
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.group_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.group_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
       //  pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 1")))
      
            {
                 error = "Failed to enter Categorie 1 :" + getData("Categorie 1");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
            // pause(3000);
             
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        
             
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))) {
          error = "Failed to select '"+testData.getData("Categorie 1")+"' from 'Categorie 1' options.";
            return false;
        }
     //   pause(5000);
        
         //Select drop click
      
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick3())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick3())){
            error = "Failed to click Profile tab.";
            return false;
                   }
     narrator.stepPassedWithScreenShot("Successfully completed Stakeholder Individual categorie 1");
       
        //Relationship owners
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.group_relationshipOwner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.group_relationshipOwner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        
        SeleniumDriverInstance.pause(3000);
        
        //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search()))
            {
                error = "Failed to wait for Relationship owner text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Relationship owner")))
            {
                 error = "Failed to enter Relationship owner option :" + getData("Relationship owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
 
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))) {
         error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' options.";
            return false;
        }
        //block this tickbox
           SeleniumDriverInstance.pause(3000);
  
        //Select after checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick4())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick4())){
            error = "Failed to click Profile tab.";
            return false;
           }
 
         SeleniumDriverInstance.pause(3000);
         
         //Accountable owner
         
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.group_accountableOwner())){
            error = "Failed to wait for 'Accountable owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.group_accountableOwner())){
            error = "Failed to click 'Accountable owner' dropdown.";
            return false;
        } 
         
       if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Accountable Owner")))
            {
                 error = "Failed to enter Accountable Owner option :" + getData("Accountable Owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
    
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to wait for Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to click Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }

        //Industry select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.groupIndustry_dropdown())){
            error = "Failed to wait for 'Industry' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.groupIndustry_dropdown())){
            error = "Failed to select '"+testData.getData("Industry")+"' from 'Industry' dropdown options.";
            return false;
        }
           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Industry")))
            {
                 error = "Failed to enter Industry option :" + getData("Industry");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
    
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Industry"))))
        {
            error = "Failed to wait for Industry drop down option : " + getData("Industry");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Industry"))))
        {
            error = "Failed to click Industry drop down option : " + getData("Industry");
            return false;
        }
        //Group description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_desc())){
            error = "Failed to wait for 'Group description' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.group_desc(), testData.getData("Group description"))){
            error = "Failed to enter '"+testData.getData("Group description")+"' into 'Group description' textarea.";
            return false;
        }
     
        //Applicable Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.applicableBU_DD())){
            error = "Failed to wait for 'Applicable Business Unit.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.applicableBU_DD())){
            error = "Failed to click 'Applicable Business Unit.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        
        //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search1()))
            {
                error = "Failed to wait for Relationship owner text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Applicable Business units")))
            {
                 error = "Failed to enter Applicable Business units :" + getData("Applicable Business units");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Applicable Business units")))){
            error = "Failed to wait for 'Applicable Business units' options.";
            return false;
        }
 
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Applicable Business units")))) {
         error = "Failed to select '"+testData.getData("Applicable Business units")+"' from 'Applicable Business units' options.";
            return false;
        }
        
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick5())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick5())){
            error = "Failed to click Profile tab.";
            return false;
           }
        
        pause(3500);
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_savetocontinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.group_savetocontinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        pause(2000);
       
        
         //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          

        return true;
    }

}
