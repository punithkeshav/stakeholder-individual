/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Engagements plan",
        createNewBrowserInstance = false
)

public class FR7_AddViewEngagementPlan_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR7_AddViewEngagementPlan_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if(!navigateToEngagementPlan()){
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully opened Engagement Record No: #" + getRecordId());
    }

    public boolean navigateToEngagementPlan(){
      
        pause(3500);
         
         //Navigate to the Enagements tab.
  
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }         
        
        //Navigate to the Enagements tab.
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementPlanTab())){
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementPlanTab())){
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }  
        
        narrator.stepPassedWithScreenShot("Successfully Navigate to the Enagements tab");
        pause(2500);
        
        //Engagement Search
             if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementSearch())){
            error = "Failed to wait for Engagement Search button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementSearch())){
            error = "Failed to click Engagement Search button";
            return false;
        }  
        narrator.stepPassedWithScreenShot("Successfully Engagement Search");
        
        pause(3500);
        
        //Engagement Search 1
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementSearch1())){
            error = "Failed to wait for Engagement Search button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementSearch1())){
            error = "Failed to click Engagement Search button";
            return false;
        }  
      
           pause(3500);
    
     
        
        narrator.stepPassedWithScreenShot("Successfully Engagement Search");
        
        
        String[] getRecordId = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getEngamentRecord()).split("#");
        setRecordId(getRecordId[1]);
        
        pause(5000);
        return true;
    }
    

}
