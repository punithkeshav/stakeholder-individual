/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Analysis",
        createNewBrowserInstance = false
)

public class UC_STA_01_03_UpdateStakeholderAnalysis_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public UC_STA_01_03_UpdateStakeholderAnalysis_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!updateStakeholderAnalysis())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Stakeholder analysis");
    }

    public boolean updateStakeholderAnalysis(){
     
        //Left chevron
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_leftarrow())){
            error = "Failed to wait for left arrow.";
            return false;
        }
        for (int i = 0; i < 6; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_leftarrow())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
         
        //Stakeholder analysis tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_analysisTab())){
            error = "Failed to wait for 'Stakeholder analysis tab' .";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_analysisTab())){
            error = "Failed to click 'Stakeholder analysis tab' .";
            return false;
        } 
        
        //Stakeholder interest dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderInterestDropdown())){
            error = "Failed to wait for stakeholder interest dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderInterestDropdown())){
            error = "Failed to click stakeholder interest dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder interest")))){
            error = "Failed to wait for stakeholder interest dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder interest")))){
            error = "Failed to select Stakeholder interest dropdown value";
            return false;
        }
        
        //Stakeholder influence dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderInfluenceDropdown())){
            error = "Failed to wait for stakeholder influence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderInfluenceDropdown())){
            error = "Failed to click stakeholder influence dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder influence")))){
            error = "Failed to wait for stakeholder influence dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder influence")))){
            error = "Failed to select Stakeholder influence dropdown value";
            return false;
        }
        
        //Stakeholder support dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderSupportDropdown())){
            error = "Failed to wait for stakeholder support dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderSupportDropdown())){
            error = "Failed to click stakeholder support dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder support")))){
            error = "Failed to wait for stakeholder support dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Stakeholder support")))){
            error = "Failed to select Stakeholder support dropdown value";
            return false;
        }
        
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commentsTextbox())){
            error = "Failed to wait for comments textbox";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.commentsTextbox(), testData.getData("Comments"))){
            error = "Failed to click comments textbox";
            return false;
        } 
        
         //Impact on Stakeholder dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.impactOnStakeholderDropdown())){
            error = "Failed to wait for Impact on Stakeholder dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.impactOnStakeholderDropdown())){
            error = "Failed to click Impact on Stakeholder dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Impact on Stakeholder parent")))){
            error = "Failed to wait for Impact on Stakeholder dropdown parent value";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Impact on Stakeholder parent")))){
            error = "Failed to select Impact on Stakeholder dropdown parent value";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checklistSelect(testData.getData("Impact on Stakeholder")))){
            error = "Failed to wait for Impact on Stakeholder dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checklistSelect(testData.getData("Impact on Stakeholder")))){
            error = "Failed to select Impact on Stakeholder dropdown value";
            return false;
        }
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for Save button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }

}
