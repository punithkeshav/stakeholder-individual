/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.Stakeholder_Individual_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Analysis OptionalScenario",
        createNewBrowserInstance = false
)
public class FR2_CaptureStakeholderAnalysis_OptionalScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR2_CaptureStakeholderAnalysis_OptionalScenario()
            
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }
    
    public TestResult executeTest()
    {
        if (!CaptureStakeholderAnalysis())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured stakeholder Analysis");
    }
    
    public boolean CaptureStakeholderAnalysis(){
     
        //Stakeholder Analysis
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholerAnalysis_Tab())){
            error = "Failed to wait for StakeHolder Analysis Tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholerAnalysis_Tab())){
            error = "Failed to click StakeHolder Analysis Tab";
            return false;
        } 
        
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Analysis Tab");
        pause(2000);
        
        //Interest dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.interestDropdown())){
            error = "Failed to wait for Interest dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.interestDropdown())){
            error = "Failed to click Interest dropdown";
            return false;
        } 

     if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder interest"))))
        {
            error = "Failed to wait for Interest dropdown drop down option : " + getData("Stakeholder interest");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder interest"))))
        {
            error = "Failed to click Interest dropdown drop down option : " + getData("Stakeholder interest");
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully clicked on Interest dropdown");
      
      pause(2000);
        //Influence dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.influenceDropdown())){
            error = "Failed to wait for influence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.influenceDropdown())){
            error = "Failed to click influence dropdown";
            return false;
        } 
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder influence"))))
        {
            error = "Failed to wait for Stakeholder influence option : " + getData("Stakeholder influence");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder influence"))))
        {
            error = "Failed to click IStakeholder influence option : " + getData("Stakeholder influence");
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully clicked on the Influence dropdown");
        //
        pause(2000);
        //Stakeholder support
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderSupportDD())){
            error = "Failed to wait for influence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderSupportDD())){
            error = "Failed to click influence dropdown";
            return false;
        } 

      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder support"))))
        {
            error = "Failed to wait for Stakeholder support: " + getData("Stakeholder support");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder support"))))
        {
            error = "Failed to click Stakeholder support : " + getData("Stakeholder support");
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully clicked on the Stakeholder support drop down");
      
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderAnalysisComments())){
            error = "Failed to wait for influence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.stakeholderAnalysisComments(),getData("Comments"))){
            error = "Failed to click influence dropdown";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully entered the comments");
        //Guidelines Panel
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.guidelinesPanel())){
            error = "Failed to wait for Guidelines Panel";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.guidelinesPanel())){
            error = "Failed to click Guidelines Panel";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully clicked the guidelines panel");
        
pause(2500);

      //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for Save button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }
}
