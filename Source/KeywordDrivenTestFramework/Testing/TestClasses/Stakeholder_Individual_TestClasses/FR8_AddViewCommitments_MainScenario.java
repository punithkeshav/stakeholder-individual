/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add view Commitments",
        createNewBrowserInstance = false
)

public class FR8_AddViewCommitments_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR8_AddViewCommitments_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if(!navigateToCommitments()){
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Search and view Commitments");
    }

    public boolean navigateToCommitments(){
 
   pause(3500);
        
        
        //Navigate to Commitments Tab
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitmentsTab1())){
            error = "Failed to wait for 'Commitments' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitmentsTab1())){
            error = "Failed to click on 'Commitments' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'commitmentsTab.");
        
         //Click Search button
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitSearch())){
            error = "Failed to wait for 'Click Search button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitSearch())){
            error = "Failed to click on 'Click Search button.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully navigated to 'Click Search button.");
         
        pause(3000);
        
        //Click Search button
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitSearch1())){
            error = "Failed to wait for 'Click Search button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitSearch1())){
            error = "Failed to click on 'Click Search button.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully navigated to 'Click Search button.");
         
        pause(3000);
        
        return true;
    }
    

}
