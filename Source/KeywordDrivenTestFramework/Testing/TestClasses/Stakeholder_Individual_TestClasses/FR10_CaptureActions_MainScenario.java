/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add Actions",
        createNewBrowserInstance = false
)

public class FR10_CaptureActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR10_CaptureActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
          
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!addActions())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual / Stakeholder Individual Action record saved.");
    }

    public boolean addActions(){
        
         
        SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.stakeholdersTab2());
       
         pause(3500);
        //navigate to right chevron
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to wait for 'right' arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to click on 'right' arrow.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to click on 'right'arrow.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to click on 'right'arrow.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to right chevron.");
        
        pause(2500);
      //Navigate to the Actions Tab 
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.actions_tab())){
            error = "Failed to wait for 'Actions Tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.actions_tab())){
            error = "Failed to click on 'Actions Tab'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Actions Tab.");
       
        pause(2500);
        
        //Add Action Button
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addActionButton())){
            error = "Failed to wait for 'Add Action Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addActionButton())){
            error = "Failed to click on 'Add Action Button'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Add Action Button.");
        pause(3500);
        
//        //switch to the another window
//          if(!SeleniumDriverInstance.switchToTabOrWindow()){
//            error = "Failed to switch on the 'commitments' window.";
//            return false;
//        }
          
          
          //Type of action
          
          
          //Action description
          
          
           //Entity
          
          //Responsible Personal
          
         
          
          //Agency
          
          
          //Action due date
          
          
          //Replicate this action to multiple users?
        //if(getData)
            
            
         //actions
         
         //Type Of Action
     pause(5000);
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.typeOfAction_dropdown()))
        {
            error = "Failed to wait for 'Type of action' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.typeOfAction_dropdown()))
        {
            error = "Failed to click Type of action dropdown.";
            return false;
        }

      pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Type of action drop down option : " + getData("Type of action");
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully click 'Type of action'.");
     
	 pause(5000);
     //Action Description
     
     
    if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.actionDescription(), getData("Action description")))
        {
            error = "Failed to enter  Action description :" + getData("Action description");
            return false;
        }
     pause(1000);


//Entity
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entityDropDown()))
        {
            error = "Failed to wait for 'Entity' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entityDropDown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 2")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Categorie 2");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 2");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 2");
            return false;
        }
        pause(2200);
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
//        pause(2200);
//      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
//        {
//            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
//            return false;
//        }
//        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
//        { 
//            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
//            return false;
//        }    
//     pause(2200);
// if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
//        {
//            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
//            return false;
//        }
// 
//  if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
//        {
//            error = "Failed to click Applicable business units option." + getData("Categorie 5");
//            return false;
//        }
//
pause(2200);
     
     //Responsible Person
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.responsibleDropDown1()))
        {
            error = "Failed to wait for 'Responsible person' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.responsibleDropDown1()))
        {
            error = "Failed to click Responsible person dropdown ";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search()))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }
		
         if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Responsible person")))
        {
            error = "Failed to enter  Responsible person :" + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

      pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        
          narrator.stepPassedWithScreenShot("Successfully click 'Responsible person'.");
     pause(3000);
     
     //Agency
     
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.agencyDropDown1()))
        {
            error = "Failed to wait for 'Agency' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.agencyDropDown1()))
        {
            error = "Failed to click Agency dropdown.";
            return false;
        }
       
         if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Agency")))
        {
            error = "Failed to enter Agency :" + getData("Agency");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

      pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Agency"))))
        {
            error = "Failed to wait for Agency drop down option : " + getData("Agency");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Agency"))))
        {
            error = "Failed to click Agency drop down option : " + getData("Agency");
            return false;
        }
     narrator.stepPassedWithScreenShot("Successfully click 'Agency'.");
     
     //Action Due Date
     
     if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.actionDueDate()))
        {
            error = "Failed to wait for action due date.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.actionDueDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");
     
        pause(4500);
     
    //Save
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.actionSave()))
        {
            error = "Failed to wait for 'Save..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.actionSave()))
        {
            error = "Failed to click save .";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully saved actions.");
        pause(3000);
     
         
         
         
         //Actions
        return true;
    }

}
