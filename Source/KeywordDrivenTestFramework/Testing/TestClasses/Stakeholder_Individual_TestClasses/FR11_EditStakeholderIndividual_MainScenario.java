/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Stakeholder Individual updates saved",
        createNewBrowserInstance = false
)

public class FR11_EditStakeholderIndividual_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR11_EditStakeholderIndividual_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
               this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH,2500);
       endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }

    public boolean navigateToStakeholderIndividual(){
         
         //Navigate to Stakeholders
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");
        
        //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to click Search Button";
            return false;
        }         
        
        pause(4500);
        
       //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to click Search Button";
            return false;
        }         
        
        pause(3030);
        
        //Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to wait for Record to be opened";
            return false;
        }
        
      if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to click  Record to be opened";
            return false;
        }
        
        pause(3000);
        
        narrator.stepPassedWithScreenShot("Successfully opened Commitments record");
       
        //Update record
          //Process flow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_fname())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
            
        }
       
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_fname(), testData.getData("First name"))){
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        } 
        
         narrator.stepPassedWithScreenShot("Successfully entered Individual first name.");
         
        //Stakeholder Individual last name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_lname())){
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_lname(), testData.getData("Last name"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual last name.");
        
        //Stakeholder Individual known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_knownas())){
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        } 
        
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual known as.");
        
        //Stakeholder Individual title dropdown
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to select '"+testData.getData("Title")+"' from 'Title' options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered 'text fields' .");
        
        //Relationship owners
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        
        SeleniumDriverInstance.pause(5000);
        
        //enter text on search box
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.text_Search()))
            {
                error = "Failed to wait for Relationship owner text box.";
                return false;
            }

           if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Relationship owner")))
            {
                 error = "Failed to enter Relationship owner option :" + getData("Relationship owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        
        //Try this
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Relationship owner")))) {
         error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' options.";
            return false;
        }
        //block this tickbox
           SeleniumDriverInstance.pause(5000);
  
        //Select after checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick())){
            error = "Failed to click Profile tab.";
            return false;
                   }
 
         SeleniumDriverInstance.pause(8000);
         
         //Accountable owner
         
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.accountableOwner())){
            error = "Failed to wait for 'Accountable owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.accountableOwner())){
            error = "Failed to click 'Accountable owner' dropdown.";
            return false;
        } 
         
       if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Accountable Owner")))
            {
                 error = "Failed to enter Accountable Owner option :" + getData("Accountable Owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
    
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to wait for Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Accountable Owner"))))
        {
            error = "Failed to click Accountable Owner drop down option : " + getData("Accountable Owner");
            return false;
        }

         
      //Stakeholder Individual categorie 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercatDD())){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercatDD())){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 1")))
      
            {
                 error = "Failed to enter Categorie 1 :" + getData("Categorie 1");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        
             
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 1")))) {
          error = "Failed to select '"+testData.getData("Categorie 1")+"' from 'Categorie 1' options.";
            return false;
        }
        pause(5000);
        
         //Select drop click
         
//         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick1())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick1())){
            error = "Failed to click Profile tab.";
            return false;
                   }
     narrator.stepPassedWithScreenShot("Successfully completed Stakeholder Individual categorie 1");
     
     
        //Applicable business units
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
         pause(3000);
        
    if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.deselectAll())){
            error = "Failed to wait for deselect all button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.deselectAll())){
            error = "Failed to click deselect all button.";
            return false;
        }
        
        pause(3500);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.applicableBusinessUnitDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 2")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Categorie 2");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 2");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 2");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
             
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
            return false;
        }    

 if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
            return false;
        }
 
  if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Categorie 5"))))
        {
            error = "Failed to click Applicable business units option." + getData("Categorie 5");
            return false;
        }
//
        pause(5000);
        
         //Drop click escape
         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick2())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick2())){
            error = "Failed to click Profile tab.";
            return false;
                   }
        
        narrator.stepPassedWithScreenShot("Successfully completed Applicable business units");
        pause(5000);
        
        //
         
         pause(5000);
        if(getData("Access Contact").equalsIgnoreCase("TRUE")){
            //Access contact Information

        //Navigate to Stakeholder Details Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to wait for 'Stakeholder Details' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to click on 'Stakeholder Details' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder Details' tab.");
        
        pause(5000);
        
         //Designation
           
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.designation())){
           error = "Failed to wait for 'Designation' input field.";
            return false;
            }
            
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.designation())){
             error = "Failed to wait for 'Designation' input field.";
             return false;
            }
            
            pause(5000);
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.designation(), getData("Designation"))){
                error = "Failed to enter '" + getData("Designation") + "' into 'Designation' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Designation : '" + getData("Designation") + "'.");
            
         //Date of Birth
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Dob())){
             error = "Date of Birth' input field.";
            return false;
            }
         //  pause(4000);
           if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Dob())){
            error = "Failed to click date of birth  dropdown.";
            return false;
           }
          // pause(4000);
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.Dob(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Date of birth.";
            return false;
        }

            narrator.stepPassedWithScreenShot("DoB : '" + getData("Dob") + "'.");
            //Gender
           
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.genderDD())){
                error = "Failed to wait for 'Gender' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.genderDD())){
                error = "Failed to wait for 'Gender' input field.";
                return false;
            }
             
      if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.textSearch(), getData("Gender"))){
                error = "Failed to enter '" + getData("Gender") + "' into 'Gender' input field.";
                return false;
            } 
            
     if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Gender"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Gender");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Gender"))))
        {
            error = "Failed to click Function drop down option : " + getData("Gender");
            return false;
        }
           
            narrator.stepPassedWithScreenShot("Gender : '" + getData("Gender") + "'.");
            
             //Nationality
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.nationalityDD())){
                error = "Failed to wait for 'Country' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.nationalityDD())){
                error = "Failed to wait for 'Country' input field.";
                return false;
            }

       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Country"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Country");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Country"))))
        {
            error = "Failed to click Function drop down option : " + getData("Country");
            return false;
        }
            narrator.stepPassedWithScreenShot("Country : '" + getData("Country") + "'.");
   

            pause(5000);
            //Primary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.primaryContactNo())){
                error = "Failed to wait for 'Primary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.primaryContactNo(), getData("Primary Contact Number"))){
                error = "Failed to enter '" + getData("Primary Contact Number") + "' into 'Primary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Primary Contact Number : '" + getData("Primary Contact Number") + "'.");

            //Secondary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo())){
                error = "Failed to wait for 'Secondary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo(), getData("Secondary Contact Number"))){
                error = "Failed to enter '" + getData("Secondary Contact Number") + "' into 'Secondary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Secondary Contact Number : '" + getData("Secondary Contact Number") + "'.");

            //Email address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.emailAddress())){
                error = "Failed to wait for 'Email address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.emailAddress(), getData("Email address"))){
                error = "Failed to enter '" + getData("Email address") + "' into 'Email address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address") + "'.");

            //Street address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.streetAddress())){
                error = "Failed to wait for 'Street address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.streetAddress(), getData("Street address"))){
                error = "Failed to enter '" + getData("Street address") + "' into 'Street address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Street address : '" + getData("Street address") + "'.");
            
         //Zip/Postal code
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_PostalCode())){
                error = "Failed to wait for 'Zip/Postal code' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_PostalCode(), getData("Zip/Postal code"))){
                error = "Failed to enter '" + getData("Zip/Postal code") + "' into 'Zip/Postal code' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Zip/Postal code : '" + getData("Zip/Postal code") + "'.");
           
            //Geographic zone
          SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.geographicZoneDD());
          
              if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.geographicZoneDD())){
                error = "Failed to wait for 'Geographic location' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.geographicZoneDD())){
                error = "Failed to wait for 'Geographic location' input field.";
                return false;
            }
             
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.textSearch(), getData("Geographic location"))){
                error = "Failed to enter '" + getData("Geographic location") + "' into 'Geographic location' input field.";
                return false;
            } 
                 
          if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.geographicSelect()))
        {
            error = "Failed to click Function drop down option : " + getData("Geographic location");
            return false;
        }
           
            narrator.stepPassedWithScreenShot("Geographic location : '" + getData("Geographic location") + "'.");



            pause(5000);
           
            //Is the correspondence address different to the street address?
            if(getData("Correspondence Address").equalsIgnoreCase("TRUE")){
                //correspondence address checkbox
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondence_Panel())){
                    error = "Failed to wait for 'correspondence address' panel.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.correspondence_Panel())){
                    error = "Failed to click on 'correspondence address' panel.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked the 'correspondence address' panel.");

                //Correspondence address textarea
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress())){
                    error = "Failed to wait for 'correspondence address' input field.";
                    return false;
                }
                if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress(), getData("correspondence address"))){
                    error = "Failed to enter '" + getData("correspondence address") + "' into 'correspondence address' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("correspondence address : '" + getData("correspondence address") + "'.");
            }

            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.corr_PostalCode())){
                error = "Failed to wait for 'Zip Code' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.corr_PostalCode(), getData("corr_PostalCode"))){
                error = "Failed to enter '" + getData("Zip Code") + "' into 'Zip Code' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Zip Code : '" + getData("Zip Code") + "'.");
        }
        
        //Donot contact checkbox
            if(getData("Donot Contact").equalsIgnoreCase("TRUE")){
                //Donot contact checkbox
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.donotContactCheckbox())){
                    error = "Failed to wait for Donot contact checkbox";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.donotContactCheckbox())){
                    error = "Failed to click on Donot contact checkbox";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked the Donot contact checkbox");

         //Alternate contact dropdown
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.alternateContactDpdn())){
            error = "Failed to wait for Alternate contact  dropdown.";
            return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.alternateContactDpdn())){
            error = "Failed to click Alternate contact  dropdown.";
            return false;
           }
          //enter the text
          if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search2(), getData("Alternate contact"))){
                error = "Failed to enter '" + getData("comments") + "' into 'comments' input field.";
                return false;
          }
          
          //select contact
          pause(4000);
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_AltContact(testData.getData("Alternate contact")))){
             error = "Failed to wait for Alternate contact  option.";
             return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_AltContact(testData.getData("Alternate contact")))){
            error = "Failed to select '"+testData.getData("Alternate contact")+"' from 'Alternate contact' option.";
            return false;
           }

           //Relationship to stakeholder dropdown
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relationshipToStakeholder())){
            error = "Failed to wait for Relationship to stakeholder dropdown.";
            return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relationshipToStakeholder())){
            error = "Failed to click Relationship to stakeholder dropdown.";
            return false;
           }
       
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Relationship to stakeholder")))){
             error = "Failed to wait for Relationship to stakeholder option.";
             return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Relationship to stakeholder")))){
            error = "Failed to select '"+testData.getData("Relationship to stakeholder")+"' from 'Relationship to stakeholder' option.";
            return false;
           }
        
                //Contact number textarea
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.contactNumber())){
                    error = "Failed to wait for 'Contact number' input field.";
                    return false;
                }
                if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.contactNumber(), getData("Contact number"))){
                    error = "Failed to enter '" + getData("Contact number") + "' into 'Contact number' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("Contact number : '" + getData("Contact number") + "'.");
            }

            
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   


            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
             String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

//          
//        SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath());
        
        //Exit
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.exitButton())){
            error = "Failed to wait for Exit button.";
            return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.exitButton())){
            error = "Failed to click Exit Button.";
            return false;
          
          }
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
       //Dialogue confirmation
       
//       SeleniumDriverInstance.switchToTabOrWindow();
//       
//       
//       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.yesButton())){
//            error = "Failed to wait for Yes button.";
//            return false;
//           }
//    
//       
//       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.yesButton())){
//            error = "Failed to click Yes Button.";
//            return false;
//          
//          }
//             pause(3500);
//             
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            
              pause(3030);
        
    //Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to wait for Record to be opened";
            return false;
        }
        
      if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to click  Record to be opened";
            return false;
        }
       pause(3000);
       
        narrator.stepPassedWithScreenShot("Successfully clicked 'on record.");
        
        pause(3000);
        
        return true;
    }

}
