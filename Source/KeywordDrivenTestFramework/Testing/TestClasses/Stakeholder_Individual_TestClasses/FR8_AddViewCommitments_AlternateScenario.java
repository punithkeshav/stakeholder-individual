/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add view Commitments",
        createNewBrowserInstance = false
)

public class FR8_AddViewCommitments_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR8_AddViewCommitments_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if(!AddCommitments()){
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("•	Commitments record viewed or saved");
    }

    public boolean AddCommitments(){
 
pause(3500);
        
        
        //Navigate to Commitments Tab
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitmentsTab1())){
            error = "Failed to wait for 'Commitments' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitmentsTab1())){
            error = "Failed to click on 'Commitments' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'commitmentsTab.");
        
         pause(3000);
         //Click Add commitments button
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addCommitmentButton())){
            error = "Failed to wait for ' Add commitments button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addCommitmentButton())){
            error = "Failed to click on  Add commitments button.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully  Add commitments button.");

        //Add commitements window

        //switch to the new window
        pause(10000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'commitments' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Add commitements' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(Stakeholder_Individual_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        
         //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addCommitmentsProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addCommitmentsProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }

        //Business unit
        
           if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitmentsBusinessUnitDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitmentsBusinessUnitDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 2")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Categorie 2");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 2");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 2");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
             
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
            return false;
        }    

 if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
        {
            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
            return false;
        }
 
  if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
        {
            error = "Failed to click Applicable business units option." + getData("Categorie 5");
            return false;
        }
//
        pause(3000);
        

        
        narrator.stepPassedWithScreenShot("Successfully completed Applicable business units");
        
        
        //Functional location
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitmentfunctionalLocationDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.commitmentfunctionalLocationDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Functional location")))
            {
                 error = "Failed to enter Functional location :" + getData("Functional location");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
  
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to click Functional location drop down option : " + getData("Functional location");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Functional location");
        
        
        
        //Commitment Register title
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.commitmentRegisterTitle())) {
            error = "Failed to wait for 'Engagement plan title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.commitmentRegisterTitle(), getData("Commitment Register title"))) {
            error = "Failed to enter '" + Stakeholder_Individual_PageObjects.commitmentRegisterTitle() + "' into Commitment Register title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + Stakeholder_Individual_PageObjects.commitmentRegisterTitle() + "' into Commitment Register title field.");
       
        //Commitments register owner
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.cRegisterOwnerDD())){
            error = "Failed to wait for Commitments register owner field";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.cRegisterOwnerDD())){
            error = "Failed to select Commitments register owner field";
            return false;
        } 
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Responsible person")))
            {
                 error = "Failed to enter Responsible person:" + getData("Responsible person");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person")))){
            error = "Failed to wait for 'Responsible person' options.";
            return false;
        }

        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person")))) {
         error = "Failed to select '"+testData.getData("Responsible person")+"' from 'Responsible person' options.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Responsible person'");
        
      

         //Strategic objectives
//         
//         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.strategicObjectivesDD())){
//            error = "Failed to wait for Strategic objectives field";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.strategicObjectivesDD())){
//            error = "Failed to select Strategic objectives field";
//            return false;
//        } 
//        
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Strategic objectives")))
//            {
//                 error = "Failed to enter Strategic objectives:" + getData("Strategic objectives");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.pressEnter())
//            {
//                error = "Failed to press enter";
//                return false;
//            }
//      
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Strategic objectives")))){
//            error = "Failed to wait for 'Strategic objectives' options.";
//            return false;
//        }
//
//        
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Strategic objectives")))) {
//         error = "Failed to select '"+testData.getData("Strategic objectives")+"' from 'Strategic objectives' options.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully entered Strategic objectives'");

         //Save and Continue
         
        pause(4500);
       
       //Save
       
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.cSaveAndContinue())){
            error = "Failed to wait for Save and Continue button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.cSaveAndContinue())){
            error = "Failed to select Save and Continue button";
            return false;
        } 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
 String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String getRecordId = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = getRecordId.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + getRecordId);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Stakeholder Individual record saved");
        
        pause(2500);
                
          //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        
        //Switch to a new tab or window
        if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver, Stakeholder_Individual_PageObjects.projectTilte())){
            error = "Failed to switch to 'IsoMetrix' window.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(Stakeholder_Individual_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }

        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully switched to 'IsoMetrix' window.");
        
       
        
        return true;
    }
}
    


    
