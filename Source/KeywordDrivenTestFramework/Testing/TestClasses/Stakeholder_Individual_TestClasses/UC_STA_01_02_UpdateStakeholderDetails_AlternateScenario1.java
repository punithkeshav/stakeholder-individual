/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Update stakeholder details Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class UC_STA_01_02_UpdateStakeholderDetails_AlternateScenario1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public UC_STA_01_02_UpdateStakeholderDetails_AlternateScenario1()
    {
        
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH,2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

   public TestResult executeTest()
    {
        
        if(!UpdateStakeholderIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean UpdateStakeholderIndividual(){
 
        pause(5000);
        if(getData("Access Contact").equalsIgnoreCase("TRUE")){
            //Access contact Information

        //Navigate to Stakeholder Details Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to wait for 'Stakeholder Details' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to click on 'Stakeholder Details' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder Details' tab.");
        
        pause(5000);
        
         //Designation
           
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.designation())){
           error = "Failed to wait for 'Designation' input field.";
            return false;
            }
            
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.designation())){
             error = "Failed to wait for 'Designation' input field.";
             return false;
            }
            
            pause(5000);
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.designation(), getData("Designation"))){
                error = "Failed to enter '" + getData("Designation") + "' into 'Designation' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Designation : '" + getData("Designation") + "'.");
            
         //Date of Birth
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Dob())){
             error = "Date of Birth' input field.";
            return false;
            }
           pause(4000);
           if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Dob())){
            error = "Failed to click date of birth  dropdown.";
            return false;
           }
           pause(4000);
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.Dob(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Date of birth.";
            return false;
        }

            narrator.stepPassedWithScreenShot("DoB : '" + getData("Dob") + "'.");
            //Gender
           
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.genderDD())){
                error = "Failed to wait for 'Gender' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.genderDD())){
                error = "Failed to wait for 'Gender' input field.";
                return false;
            }
             
      if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.textSearch(), getData("Gender"))){
                error = "Failed to enter '" + getData("Gender") + "' into 'Gender' input field.";
                return false;
            } 
            
     if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Gender"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Gender");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Gender"))))
        {
            error = "Failed to click Function drop down option : " + getData("Gender");
            return false;
        }
           
            narrator.stepPassedWithScreenShot("Gender : '" + getData("Gender") + "'.");
            
             //Nationality
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.nationalityDD())){
                error = "Failed to wait for 'Country' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.nationalityDD())){
                error = "Failed to wait for 'Country' input field.";
                return false;
            }

       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Country"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Country");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Country"))))
        {
            error = "Failed to click Function drop down option : " + getData("Country");
            return false;
        }
            narrator.stepPassedWithScreenShot("Country : '" + getData("Country") + "'.");
   

            pause(5000);
            //Primary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.primaryContactNo())){
                error = "Failed to wait for 'Primary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.primaryContactNo(), getData("Primary Contact Number"))){
                error = "Failed to enter '" + getData("Primary Contact Number") + "' into 'Primary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Primary Contact Number : '" + getData("Primary Contact Number") + "'.");

            //Secondary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo())){
                error = "Failed to wait for 'Secondary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo(), getData("Secondary Contact Number"))){
                error = "Failed to enter '" + getData("Secondary Contact Number") + "' into 'Secondary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Secondary Contact Number : '" + getData("Secondary Contact Number") + "'.");

            //Email address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.emailAddress())){
                error = "Failed to wait for 'Email address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.emailAddress(), getData("Email address"))){
                error = "Failed to enter '" + getData("Email address") + "' into 'Email address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address") + "'.");

            //Street address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.streetAddress())){
                error = "Failed to wait for 'Street address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.streetAddress(), getData("Street address"))){
                error = "Failed to enter '" + getData("Street address") + "' into 'Street address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Street address : '" + getData("Street address") + "'.");
            
         //Zip/Postal code
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_PostalCode())){
                error = "Failed to wait for 'Zip/Postal code' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_PostalCode(), getData("Zip/Postal code"))){
                error = "Failed to enter '" + getData("Zip/Postal code") + "' into 'Zip/Postal code' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Zip/Postal code : '" + getData("Zip/Postal code") + "'.");
           
            //Geographic zone
          SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.geographicZoneDD());
          
              if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.geographicZoneDD())){
                error = "Failed to wait for 'Geographic location' input field.";
                return false;
            }
            
             if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.geographicZoneDD())){
                error = "Failed to wait for 'Geographic location' input field.";
                return false;
            }
             
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.textSearch(), getData("Geographic location"))){
                error = "Failed to enter '" + getData("Geographic location") + "' into 'Geographic location' input field.";
                return false;
            } 
                 
          if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.geographicSelect()))
        {
            error = "Failed to click Function drop down option : " + getData("Geographic location");
            return false;
        }
           
            narrator.stepPassedWithScreenShot("Geographic location : '" + getData("Geographic location") + "'.");

            pause(5000);
           
            //Is the correspondence address different to the street address?
            if(getData("Correspondence Address").equalsIgnoreCase("TRUE")){
                //correspondence address checkbox
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondence_Panel())){
                    error = "Failed to wait for 'correspondence address' panel.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.correspondence_Panel())){
                    error = "Failed to click on 'correspondence address' panel.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked the 'correspondence address' panel.");

                //Correspondence address textarea
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress())){
                    error = "Failed to wait for 'correspondence address' input field.";
                    return false;
                }
                if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress(), getData("correspondence address"))){
                    error = "Failed to enter '" + getData("correspondence address") + "' into 'correspondence address' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("correspondence address : '" + getData("correspondence address") + "'.");
            }

            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.corr_PostalCode())){
                error = "Failed to wait for 'Zip Code' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.corr_PostalCode(), getData("corr_PostalCode"))){
                error = "Failed to enter '" + getData("Zip Code") + "' into 'Zip Code' input field.";
                return false;
            } 
            
            
            narrator.stepPassedWithScreenShot("Zip Code : '" + getData("Zip Code") + "'.");

        //Donot contact checkbox
            if(getData("Donot Contact").equalsIgnoreCase("TRUE")){
                //Donot contact checkbox
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.donotContactCheckbox())){
                    error = "Failed to wait for Donot contact checkbox";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.donotContactCheckbox())){
                    error = "Failed to click on Donot contact checkbox";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked the Donot contact checkbox");

         //Alternate contact dropdown
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.alternateContactDpdn())){
            error = "Failed to wait for Alternate contact  dropdown.";
            return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.alternateContactDpdn())){
            error = "Failed to click Alternate contact  dropdown.";
            return false;
           }
          //enter the text
       if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search2(), getData("Alternate contact"))){
                error = "Failed to enter '" + getData("comments") + "' into 'comments' input field.";
                return false;
          }
 
       SeleniumDriverInstance.pressEnter();
        error = "Failed to press enter ";
        
          pause(2500);
          
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Alternate contact"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Alternate contact");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Alternate contact"))))
        {
            error = "Failed to click Function drop down option : " + getData("Alternate contact");
            return false;
        }
           

           //Relationship to stakeholder dropdown
          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relationshipToStakeholder())){
            error = "Failed to wait for Relationship to stakeholder dropdown.";
            return false;
           }
          if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relationshipToStakeholder())){
            error = "Failed to click Relationship to stakeholder dropdown.";
            return false;
           }
  
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Relationship to stakeholder"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Relationship to stakeholder");
            return false;
        }
     if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Relationship to stakeholder"))))
        {
            error = "Failed to click Function drop down option : " + getData("Relationship to stakeholder");
            return false;
        }
           
        //Contact number textarea
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.contactNumber())){
                    error = "Failed to wait for 'Contact number' input field.";
                    return false;
                }
         if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.contactNumber(), getData("Contact number"))){
                    error = "Failed to enter '" + getData("Contact number") + "' into 'Contact number' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("Contact number : '" + getData("Contact number") + "'.");
            }
  //Email Addresss
  if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.corrEmailAddress())){
                    error = "Failed to wait for 'Email address' input field.";
                    return false;
                }
     if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.corrEmailAddress(), getData("Email address1"))){
                    error = "Failed to enter '" + getData("Email address") + "' into 'Contact number' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address1") + "'.");

        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }

          //  String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        
    }
return true;
}
}