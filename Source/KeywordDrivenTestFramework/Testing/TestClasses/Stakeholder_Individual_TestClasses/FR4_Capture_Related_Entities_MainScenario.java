/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Entities - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Related_Entities_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_Capture_Related_Entities_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!captureRelatedGroups()){
            return narrator.testFailed("Failed due - " + error);
        }
        
       return narrator.finalizeTest("Stakeholder Individual / Related Groups record saved.");
    }


    public boolean captureRelatedGroups(){
        pause(3500);
        //Related Groups Tab

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relatedGroupsTab())){
            error = "Failed to wait for 'Entities' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relatedGroupsTab())){
            error = "Failed to click 'Entities' tab.";
            return false;
        }        
        pause(2000);
        
        //Related Groups
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relatedGroups_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relatedGroups_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully Related Groups");
        pause(2500);
        
        //Group name
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.group_name_dropdown())){
            error = "Failed to wait for 'entity name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.group_name_dropdown())){
            error = "Failed to click 'entity name' dropdown.";
            return false;
        }    

      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Group name")))
      
            {
                 error = "Failed to enter Group name :" + getData("Group name");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Group name"))))
        {
            error = "Failed to wait for Group name drop down option : " + getData("Group name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Group name"))))
        {
            error = "Failed to click Group name drop down option : " + getData("Group name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Group name");

        //stakeholder position
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholder_positionDD())){
            error = "Failed to wait for 'stakeholder position' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholder_positionDD())){
            error = "Failed to click 'stakeholder position' dropdown.";
            return false;
        }
        

         if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Stakeholder position")))
      
            {
                 error = "Failed to enter Group name :" + getData("Stakeholder position");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder position"))))
        {
            error = "Failed to wait for Group name drop down option : " + getData("Stakeholder position");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder position"))))
        {
            error = "Failed to click Group name drop down option : " + getData("Stakeholder position");
            return false;
        }
     narrator.stepPassedWithScreenShot("Successfully captured Stakeholder position");
        
        //entities save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_save())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
        pause(5000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
