/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add Engagements - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR7_AddViewEngagementPlan_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR7_AddViewEngagementPlan_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
       
        if(!AddEngagementPlan()){
            return narrator.testFailed("Failed due - " + error);
        }
 
        return narrator.finalizeTest("Successfully Captured Engagement - record: #" + getRecordId());
    }

    
        
    
    public boolean AddEngagementPlan(){
        
        pause(3500);
         
         //Navigate to the Enagements tab.
  
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }         
        
        //Navigate to the Enagements tab.
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementPlanTab())){
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementPlanTab())){
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }  
        
        narrator.stepPassedWithScreenShot("Successfully Navigate to the Enagements tab");
        pause(2500);
        
        //Add Engagement Plan
        
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addEngagementPlanButton())){
            error = "Failed to wait for Add Engagement Plan button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addEngagementPlanButton())){
            error = "Failed to click NaAdd Engagement Plan button";
            return false;
        }  
        pause(2500);
        
          if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          pause(3500);
           narrator.stepPassedWithScreenShot("Successfully Navigate to the Enagements tab");
           
      
        //Business unit
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.businessUnitDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.businessUnitDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
         pause(3000);
         
      if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Categorie 2")))
             
            {
                 error = "Failed to enter Applicable business units :" + getData("Categorie 2");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
             //
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 2");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 2"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 2");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 3");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 3"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 3");
            return false;
        } 
             
      if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Categorie 4");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 4"))))
        { 
            error = "Failed to click Applicable business units drop down option : " + getData("Categorie 4");
            return false;
        }    

 if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
        {
            error = "Failed to wait for Applicable business units : " + getData("Categorie 5");
            return false;
        }
 
  if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Categorie 5"))))
        {
            error = "Failed to click Applicable business units option." + getData("Categorie 5");
            return false;
        }
//
        pause(3000);
        

        
        narrator.stepPassedWithScreenShot("Successfully completed Applicable business units");
        
        
        //Functional location
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.functionalLocationDD())){
            error = "Failed to wait for Applicable business units.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.functionalLocationDD())){
            error = "Failed to click Applicable business units.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Functional location")))
            {
                 error = "Failed to enter Functional location :" + getData("Functional location");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
  
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to click Functional location drop down option : " + getData("Functional location");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Functional location");
        

        //Engagement plan title
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementPlanTitle())){
                error = "Failed to wait for 'Engagement plan title' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementPlanTitle(), getData("Engagement plan title"))){
                error = "Failed to enter '" + getData("Engagement plan title") + "' into 'Primary contact number' input field.";
                return false;
            } 
      narrator.stepPassedWithScreenShot("Engagement plan title: '" + getData("Engagement plan title") + "'.");

        
        //Engagement start date
        
     if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementStartDate())){
             error = "Engagement start date' input field.";
            return false;
            }
         //  pause(4000);
           if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementStartDate())){
            error = "Failed to click Engagement start date  dropdown.";
            return false;
           }
          // pause(4000);
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementStartDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Engagement start date.";
            return false;
        }

            narrator.stepPassedWithScreenShot("DoB : '" + getData("Engagement start date") + "'.");
        
        
        
        //Show in advance
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance())){
                error = "Failed to wait for 'Show in Advance' input field.";
                return false;
            }
     if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance"))){
                error = "Failed to enter '" + getData("Show in Advance") + "' into 'Primary contact number' input field.";
                return false;
            } 
      narrator.stepPassedWithScreenShot("Show in Advance: '" + getData("Show in Advance") + "'.");

        
        //Purpose of engagement
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EpurposeOfEngagementDD())){
            error = "Failed to wait for Purpose of Engagement checklist value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.EpurposeOfEngagementDD())){
            error = "Failed to select Purpose of Engagement value";
            return false;
        } 
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Purpose of Engagement")))
            {
                 error = "Failed to enter Purpose of Engagemente :" + getData("Purpose of Engagement");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Purpose of Engagement")))){
            error = "Failed to wait for 'Purpose of Engagement' options.";
            return false;
        }
        
        //Try this
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Purpose of Engagement")))) {
         error = "Failed to select '"+testData.getData("Purpose of Engagement")+"' from 'Purpose of Engagement' options.";
            return false;
        }
        
       //Drop click escape
         
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.checkBoxTick8())){
            error = "Failed to wait for Drop click escape.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.checkBoxTick8())){
            error = "Failed to click Drop click escape.";
            return false;
                   }
        
        pause(2500);
        //Method of engagement
         
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EmethodOfEngagementDD())){
            error = "Failed to wait for Purpose of Engagement checklist value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.EmethodOfEngagementDD())){
            error = "Failed to select Purpose of Engagement value";
            return false;
        } 
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Method of engagement")))
            {
                 error = "Failed to enter Method of engagement :" + getData("Method of engagement");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Method of engagement")))){
            error = "Failed to wait for 'Method of engagement' options.";
            return false;
        }

        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Method of engagement")))) {
         error = "Failed to select '"+testData.getData("Method of engagement")+"' from 'Method of engagement' options.";
            return false;
        }
        
        //Responsible person
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EresponsiblePersonDD())){
            error = "Failed to wait for Responsible person checklist value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.EresponsiblePersonDD())){
            error = "Failed to select Responsible person value";
            return false;
        } 
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Responsible person")))
            {
                 error = "Failed to enter Responsible person:" + getData("Responsible person");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person")))){
            error = "Failed to wait for 'Responsible person' options.";
            return false;
        }

        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Responsible person")))) {
         error = "Failed to select '"+testData.getData("Responsible person")+"' from 'Responsible person' options.";
            return false;
        }
        
        
        //Comments
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance())){
                error = "Failed to wait for 'Comments' input field.";
                return false;
            }
     if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Comments"))){
                error = "Failed to enter '" + getData("Comments") + "' into 'Primary contact number' input field.";
                return false;
            } 
      narrator.stepPassedWithScreenShot("Show in Advance: '" + getData("Comments") + "'.");

      pause(3500);

      //
        
       switch (getData("Recurrence frequency"))

        {
            
            case "Once off":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                break;

            case "Daily":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

             

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");

                break;
            case "Weekly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                break;
            case "Monthly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                break;
            case "Annually":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));
                
                pause(150000);
            //recurrence tick box
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_tickBox()))
              {
            error = "Failed to wait for tick Box";
            return false;
               }
           if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_tickBox()))
              {
            error = "Failed to click on tick Box";
            return false;
              }

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Annually from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Annually from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Annually from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Annually from dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Annually from"))))
                {
                    error = "Failed to wait for Annually from option: " + getData("Annually from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Annually from"))))
                {
                    error = "Failed to click the Annually from option: " + getData("Annually from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Annually from option: " + getData("Annually from"));
                break;
            case "Quarterly":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Quarter start from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Quarter start from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Quarter start from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Quarter start from dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Quarter start from"))))
                {
                    error = "Failed to wait for Quarter start from option: " + getData("Quarter start from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Quarter start from"))))
                {
                    error = "Failed to click the Quarter start from option: " + getData("Quarter start from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Quarter start from option: " + getData("Quarter start from"));
                break;
            case "Bi-Annually":
                //Recurrence frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to wait for Recurrence frequency dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Dropdown()))
                {
                    error = "Failed to click the Recurrence frequency dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Recurrence frequency dropdown.");

                //Recurrence frequency select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to wait for Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Recurrence frequency"))))
                {
                    error = "Failed to click the Recurrence frequency option: " + getData("Recurrence frequency");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Recurrence frequency option: " + getData("Recurrence frequency"));

                //Show in Advance
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.showInAdvance()))
                {
                    error = "Failed to wait for Show in Advance field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.showInAdvance(), getData("Show in Advance")))
                {
                    error = "Failed to enter '" + getData("Start date") + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Show in Advance") + "'.");

                //End date
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.EndDate()))
                {
                    error = "Failed to wait for End date field.";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.EndDate(), endDate))
                {
                    error = "Failed to enter '" + endDate + "'.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");
                
                //On which week day frequency 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to wait for On which week day dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.DayOfTheWeek_Dropdown()))
                {
                    error = "Failed to click the On which week day dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click On which week day dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to wait for On which week day option: " + getData("On which week day");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("On which week day"))))
                {
                    error = "Failed to click the On which week day option: " + getData("On which week day");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked On which week day option: " + getData("On which week day"));
                
                //Week of month 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to wait for Week of month dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.WeekOfMonth_Dropdown()))
                {
                    error = "Failed to click the Week of month dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Week of month dropdown.");

                //On which week day select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to wait for Week of month option: " + getData("Week of month");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Week of month"))))
                {
                    error = "Failed to click the Week of month option: " + getData("Week of month");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Week of month option: " + getData("Week of month"));
                
                //Bi-Annual from 
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to wait for Bi-Annual from dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.AnnuallyFrom_Dropdown()))
                {
                    error = "Failed to click the Bi-Annual from dropdown.";
                    return false;
                }

                narrator.stepPassedWithScreenShot("Successfully click Quarter start from dropdown.");

                //Bi-Annual from select
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Bi-Annual from"))))
                {
                    error = "Failed to wait for Bi-Annual from option: " + getData("Bi-Annual from");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.RecurrenceFrequency_Option(getData("Bi-Annual from"))))
                {
                    error = "Failed to click the Bi-Annual from option: " + getData("Bi-Annual from");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Bi-Annual from option: " + getData("Bi-Annual from"));
                break;
            default:
                System.out.println(" Sorry we don't have any other options");
        }
      
       pause(4500);
       
       //Save
       
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.saveAndContinue())){
            error = "Failed to wait for Save and Continue button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.saveAndContinue())){
            error = "Failed to select Save and Continue button";
            return false;
        } 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
 String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String getRecordId = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = getRecordId.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + getRecordId);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Stakeholder Individual record saved");
        
        pause(2500);
       return true; 
        
    }


}
