/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Stakeholder Individual record deleted",
        createNewBrowserInstance = false
)

public class FR12_DeleteStakeholderIndividual_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR12_DeleteStakeholderIndividual_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!deleteStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully deleted Stakeholder record ");
    }

    public boolean deleteStakeholderIndividual(){
         
           
         //Navigate to Stakeholders
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");
        
        //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchOptions())){
            error = "Failed to click Search Button";
            return false;
        }         
        
        pause(4500);
        
       //Search
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to wait for Search Button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchButton1())){
            error = "Failed to click Search Button";
            return false;
        }         
        
        pause(3030);
        
        //Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to wait for Record to be opened";
            return false;
        }
        
      if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.selectRecord())){
            error = "Failed to click  Record to be opened";
            return false;
        }
        
        pause(3000);
        
        narrator.stepPassedWithScreenShot("Successfully opened Commitments record");
        
        //delete record
        
        
     if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.deleteRecordButton())){
            error = "Failed to wait for delete button.";
            return false;
           }
    
       
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.deleteRecordButton())){
            error = "Failed to click delete Button.";
            return false;
          
          }
             pause(3500);
             
           narrator.stepPassedWithScreenShot("Successfully clicked delete button");
             
       SeleniumDriverInstance.switchToTabOrWindow();
       
       //Are you sure you want to delete this record?
       
       pause(3500);
   if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.yesButton1())){
            error = "Failed to wait for Yes button.";
            return false;
           }
    
       
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.yesButton1())){
            error = "Failed to click Yes Button.";
            return false;
          
          }
       narrator.stepPassedWithScreenShot("Successfully clicked yes button");
       
         pause(3500);
       
              SeleniumDriverInstance.switchToTabOrWindow();
              
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.okButton())){
            error = "Failed to wait for Yes button.";
            return false;
           }
    
       
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.okButton())){
            error = "Failed to click Yes Button.";
            return false;
          
          }
        narrator.stepPassedWithScreenShot("Successfully Record has successfully been deleted");
        pause(3500);
       
        return true;
    }

}
