/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture and updated Related Stakeholders - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!CaptureandUpdateRelatedStakeholders()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    
    public boolean CaptureandUpdateRelatedStakeholders(){
        
        //Navigate to the Relationships tab.
        //relationships tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to wait for 'relationships' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to click 'relationships' tab.";
            return false;
        }        
        pause(2000);
        //relationships add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.r_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.r_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }
        
        SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.s_name_dropdown());
        //Stakeholder name dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.s_name_dropdown())){
            error = "Failed to wait for 'Stakeholder name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.s_name_dropdown())){
            error = "Failed to click 'Stakeholder name' dropdown.";
            return false;
        }      
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search(), getData("Stakeholder Name")))
      
            {
                 error = "Failed to enter Stakeholder Name :" + getData("Stakeholder Name");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
             
       if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder Name"))))
        {
            error = "Failed to wait for Stakeholder Name drop down option : " + getData("Stakeholder Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder Name"))))
        {
            error = "Failed to click Stakeholder Name drop down option : " + getData("Stakeholder Name");
            return false;
        }
        //Stakeholder Type of Relationship
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.tor_dropdown())){
            error = "Failed to wait for 'Type of Relationship' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.tor_dropdown())){
            error = "Failed to click 'Type of Relationship' dropdown.";
            return false;
        } 
        
     if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Type of Relationship")))
      
            {
                 error = "Failed to enter Type of Relationship :" + getData("Type of Relationship");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
             pause(3000);
//         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Type of Relationship"))))
//        {
//            error = "Failed to wait for Type of Relationship drop down option : " + getData("Type of Relationship");
//            return false;
//        }


        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text3()))
        {
            error = "Failed to click Type of Relationship drop down option : " + getData("Type of Relationship");
            return false;
        }
        //relationships save
         pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.r_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.r_save())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Sucessfully saved related stakeholder record");

        
        return true;
    }

}
