/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.Stakeholder_Individual_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Engagements record viewed or saved",
        createNewBrowserInstance = false
)

public class FR9_AddView_Grievances_MainScenario extends BaseClass{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR9_AddView_Grievances_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        
    }
    
     public TestResult executeTest()
    {

        if(!AddViewGrievances()){
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Engagements record viewed or saved");
    }
     
    public boolean AddViewGrievances()
    
    {
        
       SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.stakeholdersTab2());
       
         pause(4500);
        //navigate to left chevront
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to wait for 'left' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to click on 'left' tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.right())){
            error = "Failed to click on 'Related grievances' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Related grievances Tab.");
    
        pause(1500);
        //Navigate to Related grievances Tab
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.grievancesTab())){
            error = "Failed to wait for 'Related grievances' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.grievancesTab())){
            error = "Failed to click on 'Related grievances' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Related grievances Tab.");

        //Click Search button
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.grievancesSearch())){
            error = "Failed to wait for 'Click Search button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.grievancesSearch())){
            error = "Failed to click on 'Click Search button.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully navigated to 'Click Search button.");
         
       
  
         pause(3500);
        return true;
    }
}
